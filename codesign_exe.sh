#!/bin/bash

cd target
exe=$(ls Transkribus.exe | head -n 1)
if [ -z "$exe" ]; then
    echo "No exe found to sign!"
    exit 1
fi
echo "Signing $exe"

signtool sign /sha1 4cdcb0221580009fae8365a74cb0b96353e6e3db /fd sha256 /a /tr http://ts.ssl.com /td sha256 $exe
