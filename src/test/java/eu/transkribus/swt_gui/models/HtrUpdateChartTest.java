package eu.transkribus.swt_gui.models;

import eu.transkribus.client.connection.TrpServerConn;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.rest.TrpModelMetadataList;
import eu.transkribus.swt_gui.TestApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class HtrUpdateChartTest {
	private static final Logger logger = LoggerFactory.getLogger(HtrUpdateChartTest.class);
	
	public static void main(String[] args) {
		if(args.length == 0) {
			throw new IllegalArgumentException("No arguments.");
		}
		final TestApplicationWindow test = new TestApplicationWindow(TrpServerConn.PROD_SERVER_URI, args[0], args[1], 575) {
			ModelsComposite comp;
			List<TrpModelMetadata> htrs;
			@Override
			protected void createTestContents(Composite parent) throws Exception {
				getShell().setSize(500, 700);
				comp = new ModelsComposite(parent, SWT.NONE);
				parent.pack();

				TrpModelMetadataList list = getStorage().getConnection().getModelCalls().getModels("text", null, 0, -1, null, null,
						null, null, null, null, null, null, 0, -1, "modelId", "desc");
				htrs = list.getList();
				comp.getModelTableWidget().refreshList();
				
				List<TrpModelMetadata> badHtrs = new LinkedList<>();
				
				for(TrpModelMetadata htr : htrs) {
					try {
						comp.updateDetails(htr);
					} catch (ArrayIndexOutOfBoundsException e) {
						badHtrs.add(htr);
					}
				}
				
				logger.info("Found {} bad HTRs", badHtrs.size());
				logger.info("IDs = " + badHtrs.stream().map(h -> "" + h.getModelId()).collect(Collectors.joining(", ")) );
			}
		};
		test.show();
	}
}
