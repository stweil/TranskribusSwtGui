package eu.transkribus.swt_gui.dialogs;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import eu.transkribus.core.model.beans.TrpHtr;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.DesktopUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledCombo;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.models.ModelChooserButton;

public class TranskribusLaConfigDialog extends ALaConfigDialog {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TranskribusLaConfigDialog.class);
	
	static final String HELP_WIKI_PAGE = "https://transkribus.eu/wiki/index.php/Layout_Analysis_Help";
	static final String NEURAL_NET_LBL = "Model:";
	
	private Button defaultsButton;
	private Button helpButton;
//	private Button deleteTextCheck;
	private ModelChooserButton modelChooserBtn;
	private Group baselineSettingsGroup, clusteringSetttingsGroup;
	private LabeledText minPathLength, binThresh, sepThresh, maxDistFraction, clusterDistFraction, scaling;
	private LabeledCombo minPathLengthCombo, binThreshCombo, sepThreshCombo, maxDistFractionCombo, clusterDistFractionCombo, scalingCombo;
	private LabeledCombo clusteringMethodCombo, legacyClusteringTypeCombo; 

	private LabeledCombo sizeCombo;
	private LabeledText size, tileSize;
	private Button btnTileImages;

	private CTabFolder settingsTabFolder;
	private CTabItem simpleItem, advancedItem;
	
	private JobImpl jobImpl;
	private String provider;
	
	public static final int DEFAULT_MIN_PATH_LENGTH = 25;
	public static final int DEFAULT_BIN_THRESH = 55;
	public static final int DEFAULT_SEP_THRESH = -1;
	public static final double DEFAULT_MAX_DIST_FRACTION = 0.01;
	public static final double DEFAULT_CLUSTER_DIST_FRACTION = 1.0;
	public static final double DEFAULT_SCALING = 1.0;	

	// nextgen pars:
	public static final int DEFAULT_SIZE = 1024;
	public static final int MAX_SIZE = 1200;
	public static final int DEFAULT_TILE_SIZE = 2048;
	public static final boolean DEFAULT_TILE_IMAGES = true;


	public static final String CUSTOM_ITEM = "Custom";
	public static final String PARAM_VALUE_LBL = "Param-Value: ";
	
	public TranskribusLaConfigDialog(Shell parent, ParameterMap parameters, JobImpl jobImpl) {
		super(parent, parameters);
		this.jobImpl = jobImpl;	
		this.provider = ModelUtil.PROVIDER_CITLAB_BASELINES;
		if (this.jobImpl.equals(JobImpl.TranskribusLaJobNextGen)) {
			this.provider = ModelUtil.PROVIDER_TRANSKRIBUS_BASELINES;
		}
	}

	public boolean isNextGen() {
		return this.jobImpl.equals(JobImpl.TranskribusLaJobNextGen);
	}
	
	public static TrpModelMetadata getDefaultModelHomo() {
		TrpModelMetadata model = new TrpModelMetadata();
		model.setType(TrpHtr.TYPE_LAYOUT);
		model.setModelId( (Storage.i()!=null && !Storage.i().isLoggedInAtTestServer()) ? 49271 : 3771);
		model.setName("Horizontal Line Orientation");
		return model;
	}
	
	public static TrpModelMetadata getDefaultModelHetero() {
		TrpModelMetadata model = new TrpModelMetadata();
		model.setType(TrpHtr.TYPE_LAYOUT);
		model.setModelId( (Storage.i()!=null && !Storage.i().isLoggedInAtTestServer()) ? 49272 : 3772);
		model.setName("Mixed Line Orientation");
		return model;
	}	

	public static TrpModelMetadata getDefaultModelNextGen() {
		TrpModelMetadata model = new TrpModelMetadata();
		model.setType(TrpHtr.TYPE_LAYOUT);
		model.setModelId( (Storage.i()!=null && !Storage.i().isLoggedInAtTestServer()) ? 56515 : 4605); // TODO: add model on prod server!
		model.setName("TranskribusBaselines_v0.1_scale_crop_rot90");
		return model;

	}
	
	public TrpModelMetadata getDefaultModel() {
		if (isNextGen()) {
			return getDefaultModelNextGen();
		}
		else {
			return getDefaultModelHetero();
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		
		modelChooserBtn = new ModelChooserButton(cont, true, ModelUtil.TYPE_LAYOUT, this.provider, "Selected model: ");
		modelChooserBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		logger.debug("setting default model: "+getDefaultModel());
		modelChooserBtn.setModel(getDefaultModel());
		
//		settingsGroup = new Group(cont, SWT.NONE);
//		settingsGroup.setText("Settings");
//		settingsGroup.setLayout(new GridLayout(1, false));
//		settingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		
//		settingsTabFolder = new CTabFolder(cont, 0);
//		settingsTabFolder.setLayout(new GridLayout(1, false));
//		settingsTabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//		simpleItem = initSimpleSettingsTabItem();
//		advancedItem = initAdvancedSettingsTabItem();
//		settingsTabFolder.setSelection(simpleItem);
//		SWTUtil.setTabFolderBoldOnItemSelection(settingsTabFolder);
		
		initSettingsWidget(cont);
		
		// add listener:
		addComboLabeldTextPairSettingsListener(minPathLengthCombo, minPathLength);
		addComboLabeldTextPairSettingsListener(binThreshCombo, binThresh);
		addComboLabeldTextPairSettingsListener(sepThreshCombo, sepThresh);
		addComboLabeldTextPairSettingsListener(maxDistFractionCombo, maxDistFraction);
		addComboLabeldTextPairSettingsListener(clusterDistFractionCombo, clusterDistFraction);
		addComboLabeldTextPairSettingsListener(scalingCombo, scaling);
		addComboLabeldTextPairSettingsListener(sizeCombo, size);
		
		applyParameterMapToDialog();
		
		cont.pack();
		
		return cont;
	}
	
	private void initSettingsWidget(Composite parent) {
		baselineSettingsGroup = new Group(parent, SWT.NONE);
		baselineSettingsGroup.setText("Baseline detection settings");
		baselineSettingsGroup.setLayout(new GridLayout(2, false));
		baselineSettingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		
		minPathLengthCombo = new LabeledCombo(baselineSettingsGroup, "Minimal baseline length");
		minPathLengthCombo.setToolTipText("The minimum length for a baseline");
		minPathLengthCombo.setItems(new String[] { "Low", "Medium", "High", CUSTOM_ITEM });
		setDefaultValuesAsData(minPathLengthCombo, 
				DEFAULT_MIN_PATH_LENGTH-15, DEFAULT_MIN_PATH_LENGTH,  DEFAULT_MIN_PATH_LENGTH+15);
		minPathLengthCombo.select(1);
		minPathLengthCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		minPathLength = new LabeledText(baselineSettingsGroup, PARAM_VALUE_LBL);
		minPathLength.setText(""+DEFAULT_MIN_PATH_LENGTH);
		minPathLength.setToolTipText("The minimum length for a baseline");
		minPathLength.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));		
		
		binThreshCombo = new LabeledCombo(baselineSettingsGroup, "Baseline accuracy threshold");
		binThreshCombo.setToolTipText("The threshold for binarization of the baselines mask image.");
		binThreshCombo.setItems(new String[] { "Low", "Medium", "High", CUSTOM_ITEM });
		setDefaultValuesAsData(binThreshCombo, 
				DEFAULT_BIN_THRESH-35, DEFAULT_BIN_THRESH,  DEFAULT_BIN_THRESH+35);		
		binThreshCombo.select(1);
		binThreshCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		binThresh = new LabeledText(baselineSettingsGroup, PARAM_VALUE_LBL);
		binThresh.setText(""+DEFAULT_BIN_THRESH);
		binThresh.setToolTipText("The threshold for binarization of the baselines mask image. Higher values enforce higher accuracy in the detected baselines. Ranges between 0 and 255.");
		binThresh.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));			
		
		if (!isNextGen()) {
			sepThreshCombo = new LabeledCombo(baselineSettingsGroup, "Use trained separators");
			sepThreshCombo.setItems(new String[] { "No", "Sometimes", "Always", CUSTOM_ITEM });
			setDefaultValuesAsData(sepThreshCombo, 
					DEFAULT_SEP_THRESH, 5, 125);		
			sepThreshCombo.select(0);
			sepThreshCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			sepThresh = new LabeledText(baselineSettingsGroup, PARAM_VALUE_LBL);
			sepThresh.setText(""+DEFAULT_SEP_THRESH);
			sepThresh.setToolTipText("Threshold for using the separator images. Ranges between 0 and 255. If threshold is exceeded, nearby baselines are getting merged. If -1, separators are not used at all. Ranges between 0 and 255.");
			sepThresh.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}		
		
		maxDistFractionCombo = new LabeledCombo(baselineSettingsGroup, "Max-dist for merging baselines");
		maxDistFractionCombo.setToolTipText("Maximum distance between two nearby baselines to be merged.");
		maxDistFractionCombo.setItems(new String[] { "Low", "Medium", "High", CUSTOM_ITEM });
		setDefaultValuesAsData(maxDistFractionCombo, 
				0.5*DEFAULT_MAX_DIST_FRACTION, DEFAULT_MAX_DIST_FRACTION, 5*DEFAULT_MAX_DIST_FRACTION);		
		maxDistFractionCombo.select(1);
		maxDistFractionCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		maxDistFraction = new LabeledText(baselineSettingsGroup, PARAM_VALUE_LBL);
		maxDistFraction.setText(""+DEFAULT_MAX_DIST_FRACTION);
		maxDistFraction.setToolTipText("If the distance exceeds this fraction of the *width* of the image, baselines will *not* get merged.");
		maxDistFraction.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));	

		if (!isNextGen()) {
			scalingCombo = new LabeledCombo(baselineSettingsGroup, "Image scaling");
			scalingCombo.setToolTipText("How should lines be clustered to text-regions, i.e. how many text-regions should be produced?");
			scalingCombo.setItems(new String[] { "Scale-Up (low-res imgs)", "Default", "Scale-Down (high-res-imgs)", CUSTOM_ITEM });
			setDefaultValuesAsData(scalingCombo, 
					2.0, DEFAULT_SCALING, 0.5);	
			scalingCombo.select(1);
			scalingCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			scaling = new LabeledText(baselineSettingsGroup, PARAM_VALUE_LBL);
			scaling.setText(""+DEFAULT_SCALING);
			scaling.setToolTipText("Scaling of the input image - scaling up images can be helpful when dealing with low-resolution images and vice versa.");
			scaling.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}

		if (isNextGen()) {
			sizeCombo = new LabeledCombo(baselineSettingsGroup, "Image size");
			sizeCombo.setToolTipText("The final size that image is scaled to");
			// sizeCombo.setItems(new String[] { "Small", "Medium", "Large", CUSTOM_ITEM });
			// setDefaultValuesAsData(sizeCombo, 
					// DEFAULT_SIZE/2, DEFAULT_SIZE, DEFAULT_SIZE*2);
			// sizeCombo.select(1);
			sizeCombo.setItems(new String[] { "Default", CUSTOM_ITEM });
			setDefaultValuesAsData(sizeCombo, DEFAULT_SIZE);
			sizeCombo.select(0);
			
			sizeCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			size = new LabeledText(baselineSettingsGroup, "Image size");
			size.setToolTipText("The final size that image is scaled to");
			size.setText(""+DEFAULT_SIZE);
			size.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			size.setEnabled(false);

			btnTileImages = new Button(baselineSettingsGroup, SWT.CHECK);
			btnTileImages.setText("Tile images");
			btnTileImages.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			btnTileImages.setToolTipText("Whether the image will be tiled into smaller images of the given size. Can be helpful when dealing with large images with lots of textlines, e.g. newspaper pages, but also increases the runtime.");
			btnTileImages.setSelection(DEFAULT_TILE_IMAGES);

			tileSize = new LabeledText(baselineSettingsGroup, "Tile size");
			tileSize.setToolTipText("The size of the tiles");
			tileSize.setText(""+DEFAULT_TILE_SIZE);
			tileSize.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			tileSize.setEnabled(btnTileImages.getSelection());

			SWTUtil.onSelectionEvent(btnTileImages, e -> {
				tileSize.setEnabled(btnTileImages.getSelection());
			});
		}
		
		clusteringSetttingsGroup = new Group(parent, SWT.NONE);
		clusteringSetttingsGroup.setText("Region detection settings");
		clusteringSetttingsGroup.setLayout(new GridLayout(2, false));
		clusteringSetttingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		
		clusteringMethodCombo = new LabeledCombo(clusteringSetttingsGroup, "Method");
		clusteringMethodCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		clusteringMethodCombo.setItems(new String[] {"General", "Custom"});
		clusteringMethodCombo.setData("General", "legacy");
		clusteringMethodCombo.setData("Custom", "custom");
		clusteringMethodCombo.setToolTipText("'General' -> legacy 'left-to-right' clustering of baselines\n"
				+ "'Custom' -> simple agglomerative clustering based on a distance threshold");
		clusteringMethodCombo.select(0);
		SWTUtil.onSelectionEvent(clusteringMethodCombo.getCombo(), e -> updateUi());
		
		legacyClusteringTypeCombo = new LabeledCombo(clusteringSetttingsGroup, "Baseline orientation");
		legacyClusteringTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		legacyClusteringTypeCombo.setItems(new String[] {"Horizontal", "Mixed"});
		legacyClusteringTypeCombo.setToolTipText("'Horizontal' -> lines are assumed to be horizontal.\n"
				+ "'Mixed' -> lines are assumed to be rotated 0°, 90°, 180° or 270°");
		legacyClusteringTypeCombo.select(0);
		setDefaultValuesAsData(legacyClusteringTypeCombo, "default", "hom");
		
		clusterDistFractionCombo = new LabeledCombo(clusteringSetttingsGroup, "Nr of Text-regions");
		clusterDistFractionCombo.setToolTipText("How should lines be clustered to text-regions, i.e. how many text-regions should be produced?");
		clusterDistFractionCombo.setItems(new String[] { "One", "Few", "Medium", "Many", CUSTOM_ITEM });
		setDefaultValuesAsData(clusterDistFractionCombo, 
				-1, 1.6, DEFAULT_CLUSTER_DIST_FRACTION, 0.4);	
		clusterDistFractionCombo.select(2);
		clusterDistFractionCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		clusterDistFraction = new LabeledText(clusteringSetttingsGroup, PARAM_VALUE_LBL);
		clusterDistFraction.setText(""+DEFAULT_CLUSTER_DIST_FRACTION);
		clusterDistFraction.setToolTipText("If the distance exceeds this fraction of the *width* of the image, baselines will *not* get clustered to regions. If set to <= 0, no region clustering will be performed.");
		clusterDistFraction.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		updateUi();
	}
	
	private void updateUi() {
		// update visibility of cluster method pars:
		if (clusteringMethodCombo.getSelectionIndex() == 0) { // Legacy method selected
			legacyClusteringTypeCombo.setParent(clusteringSetttingsGroup);
			legacyClusteringTypeCombo.moveBelow(clusteringMethodCombo);
			clusterDistFractionCombo.setParent(SWTUtil.dummyShell);
			clusterDistFraction.setParent(SWTUtil.dummyShell);
		}
		else {
			legacyClusteringTypeCombo.setParent(SWTUtil.dummyShell);
			clusterDistFractionCombo.setParent(clusteringSetttingsGroup);
			clusterDistFractionCombo.moveBelow(clusteringMethodCombo);
			clusterDistFraction.setParent(clusteringSetttingsGroup);
			clusterDistFraction.moveBelow(clusterDistFractionCombo);
		}
		clusteringSetttingsGroup.layout();
	}
	
	private String getValue(LabeledCombo combo, LabeledText text) {
		if (!combo.getSelectedItem().equals(CUSTOM_ITEM)) {
			return combo.getSelectedData()+"";
		}
		else {
			return text.getText();
		}
	}

	public Integer getIntegerValue(LabeledCombo combo, LabeledText text) {
		try {
			return Integer.parseInt(getValue(combo, text));
		} catch (Exception e) {
			return null;
		}
	}

	public Double getDoubleValue(LabeledCombo combo, LabeledText text) {
		try {
			return Double.parseDouble(getValue(combo, text));
		} catch (Exception e) {
			return null;
		}
	}
	
	private void setDefaultValuesAsData(LabeledCombo c, Object ...vals) {
		for (int i=0; i<vals.length; ++i) {
			if (!c.getItem(i).equals(CUSTOM_ITEM)) {
				c.setData(c.getItem(i), vals[i]);	
			}
		}
	}
	
	private String getItemForData(LabeledCombo c, Object data, String defaultVal) {
		for (String item : c.combo.getItems()) {
			if (data.equals(c.getData(item))) {
				return item;
			}
		}
		return defaultVal;
	}
	
	/**
	 * Combines the values from the given simple-par combo with the text field in the advanced settings 
	 */
	private void addComboLabeldTextPairSettingsListener(LabeledCombo combo, LabeledText text) {
		if (combo == null || text == null) {
			return;
		}

		SWTUtil.onSelectionEvent(combo.getCombo(), e -> {
			if (!combo.getSelectedItem().equals(CUSTOM_ITEM)) {
				Object val = combo.getData(combo.getSelectedItem());
				text.setText(""+val);
				text.setEnabled(false);
			}
			else {
				text.setEnabled(true);
			}
		});
	}
	
	public static boolean isConfigurable(JobImpl jobImpl) {
		return JobImpl.TranskribusLaJob.equals(jobImpl) || JobImpl.TranskribusLaJobNextGen.equals(jobImpl);
	}

	@Override
	protected void storeSelectionInParameterMap() {
		parameters = new ParameterMap();
		TrpModelMetadata model = modelChooserBtn.getModel();
		logger.debug("storeSelectionInParameterMap, model = "+model);
//		TrpModelMetadata model = resolveSelectedNetFromParameters();
		if (model == null) { // should not happen here...
			model = getDefaultModel();
		}
		
		parameters.addParameter(JobConst.PROP_MODEL_ID, model.getModelId());
		parameters.addParameter(JobConst.PROP_MODELNAME, model.getName());	
		
		// set other parameters here:
		// min_path_length=20, bin_thresh=55, sep_thresh=None, max_dist_fraction=0.1, cluster_dist_fraction=0.3
		Integer minPathLengthVal = getIntegerValue(minPathLengthCombo, minPathLength);
		if (minPathLengthVal == null) {
			throw new IllegalArgumentException("Illegal value for "+minPathLength.getLblTxt());
		}
		else {
			parameters.addParameter(JobConst.PROP_PARS_PREFIX+"min_path_length", minPathLengthVal);
		}
		
		Integer binThreshVal = getIntegerValue(binThreshCombo, binThresh);
		if (binThreshVal == null) {
			throw new IllegalArgumentException("Illegal value for "+binThresh.getLblTxt());
		}
		else {
			parameters.addParameter(JobConst.PROP_PARS_PREFIX+"bin_thresh", binThreshVal);
		}
		
		if (!isNextGen()) {
			Integer sepThreshVal = getIntegerValue(sepThreshCombo, sepThresh);
			if (sepThreshVal == null) {
				throw new IllegalArgumentException("Illegal value for "+sepThresh.getLblTxt());
			}
			else {
				parameters.addParameter(JobConst.PROP_PARS_PREFIX+"sep_thresh", sepThreshVal);
			}	
		}
		
		Double maxDistFractionVal = getDoubleValue(maxDistFractionCombo, maxDistFraction);
		if (maxDistFractionVal == null) {
			throw new IllegalArgumentException("Illegal value for "+maxDistFraction.getLblTxt());
		}
		else {
			parameters.addParameter(JobConst.PROP_PARS_PREFIX+"max_dist_fraction", maxDistFractionVal);
		}	
		
		String clusteringMethod = (String) clusteringMethodCombo.getSelectedData();
		parameters.addParameter(JobConst.PROP_PARS_PREFIX+"clustering_method", clusteringMethod);
		
		String clusteringLegacyType = (String) legacyClusteringTypeCombo.getSelectedData();
		parameters.addParameter(JobConst.PROP_PARS_PREFIX+"clustering_legacy_type", clusteringLegacyType);
		
		Double clusterDistFractionVal = getDoubleValue(clusterDistFractionCombo, clusterDistFraction);
		if (clusterDistFractionVal == null) {
			throw new IllegalArgumentException("Illegal value for "+clusterDistFraction.getLblTxt());
		}
		else {
			parameters.addParameter(JobConst.PROP_PARS_PREFIX+"cluster_dist_fraction", clusterDistFractionVal);
		}
		
		if (!isNextGen()) {
			Double scalingVal = getDoubleValue(scalingCombo, scaling);
			if (scalingVal == null) {
				throw new IllegalArgumentException("Illegal value for "+scaling.getLblTxt());
			}
			else {
				parameters.addParameter(JobConst.PROP_PARS_PREFIX+"scale", scalingVal);
			}		
		}

		if (isNextGen()) {
			Integer sizeVal = getIntegerValue(sizeCombo, size);
			if (sizeVal == null) {
				throw new IllegalArgumentException("Illegal value for "+size.getLblTxt());
			}
			else if (sizeVal > MAX_SIZE) {
				throw new IllegalArgumentException("Image size cannot execeed "+MAX_SIZE);
			}
			else {
				parameters.addParameter(JobConst.PROP_PARS_PREFIX+"size", sizeVal);
			}

			Integer tileSizeVal = tileSize.toIntVal(-1);
			if (tileSizeVal <= 0) {
				throw new IllegalArgumentException("Illegal value for "+tileSize.getLblTxt());
			}
			else {
				parameters.addParameter(JobConst.PROP_PARS_PREFIX+"tile_size", tileSizeVal);
			}

			if (btnTileImages.getSelection()) {
				parameters.addParameter(JobConst.PROP_PARS_PREFIX+"tile_images", "");
			}
			else {
				parameters.remove(JobConst.PROP_PARS_PREFIX+"tile_images");
			}
		}
	}

	@Override
	protected void applyParameterMapToDialog() {
		TrpModelMetadata model = resolveSelectedNetFromParameters();
		logger.debug("resolved model: "+model);
		modelChooserBtn.setModel(model);
		
		this.minPathLength.setText(""+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"min_path_length", DEFAULT_MIN_PATH_LENGTH));
		setSimpleComboValue(minPathLengthCombo, minPathLength);
		
		this.binThresh.setText(""+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"bin_thresh", DEFAULT_BIN_THRESH));
		setSimpleComboValue(binThreshCombo, binThresh);
		
		if (!isNextGen()) {
			this.sepThresh.setText(""+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"sep_thresh", DEFAULT_SEP_THRESH));
			setSimpleComboValue(sepThreshCombo, sepThresh);
		}
		
		this.maxDistFraction.setText(""+parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"max_dist_fraction", DEFAULT_MAX_DIST_FRACTION));
		setSimpleComboValue(maxDistFractionCombo, maxDistFraction);
		
		if (!isNextGen()) {
			this.scaling.setText(""+parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"scale", DEFAULT_SCALING));
			setSimpleComboValue(scalingCombo, scaling);
		}

		if (isNextGen()) {
			this.size.setText(""+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"size", DEFAULT_SIZE));
			setSimpleComboValue(sizeCombo, size);

			boolean tileImgs = parameters.containsKey(JobConst.PROP_PARS_PREFIX+"tile_images");
			btnTileImages.setSelection(tileImgs);
			this.tileSize.setText(""+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"tile_size", DEFAULT_TILE_SIZE));
			this.tileSize.setEnabled(tileImgs);
		}
		
		setSimpleComboValue(clusteringMethodCombo, parameters.getParameterValue(JobConst.PROP_PARS_PREFIX+"clustering_method", "legacy"));
		
		setSimpleComboValue(legacyClusteringTypeCombo, parameters.getParameterValue(JobConst.PROP_PARS_PREFIX+"clustering_legacy_type", "default"));
		
		this.clusterDistFraction.setText(""+parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"cluster_dist_fraction", DEFAULT_CLUSTER_DIST_FRACTION));
		setSimpleComboValue(clusterDistFractionCombo, clusterDistFraction);

		updateUi();
	}
	
	private void setSimpleComboValue(LabeledCombo combo, String text) {
		logger.info("text = "+text);
		for (int i=0; i<combo.getItems().length; ++i) {
			String item = combo.getItem(i);
			Object data = combo.getData(item);
			String dataStr = data!=null ? data.toString() : item;
			logger.info("dataStr = "+dataStr+", item = "+item);
			if (dataStr.equals(text)) {
				combo.select(i);
				return;
			}
		}
		combo.select(0);
	}
	
	private void setSimpleComboValue(LabeledCombo combo, LabeledText text) {
		for (int i=0; i<combo.getItems().length-1; ++i) {
			String item = combo.getItem(i);
			if (combo.getData(item).toString().equals(text.getText())) {
				// simple value is set:
				combo.select(i);
				text.setEnabled(false);
				return;
			}
		}
		// custom value:
		text.setEnabled(true);
		combo.select(combo.getItems().length-1);
		return;
	}
	
	private void setDefaultValues() {
		modelChooserBtn.setModel(getDefaultModel());
		
		this.minPathLength.setText(""+DEFAULT_MIN_PATH_LENGTH);
		this.minPathLengthCombo.select(1);
		setSimpleComboValue(minPathLengthCombo, minPathLength);
		
		this.binThresh.setText(""+DEFAULT_BIN_THRESH);
		this.binThreshCombo.select(1);
		setSimpleComboValue(binThreshCombo, binThresh);
		
		if (!isNextGen()) {
			this.sepThresh.setText(""+DEFAULT_SEP_THRESH);
			this.sepThreshCombo.select(0);
			setSimpleComboValue(sepThreshCombo, sepThresh);
		}
		
		this.maxDistFraction.setText(""+DEFAULT_MAX_DIST_FRACTION);
		this.maxDistFractionCombo.select(1);
		setSimpleComboValue(maxDistFractionCombo, maxDistFraction);
		
		if (!isNextGen()) {
			this.scaling.setText(""+DEFAULT_SCALING);
			this.scalingCombo.select(1);
			setSimpleComboValue(scalingCombo, scaling);		
		}

		if (isNextGen()) {
			this.size.setText(""+DEFAULT_SIZE);
			this.sizeCombo.select(1);
			setSimpleComboValue(sizeCombo, size);

			this.btnTileImages.setSelection(DEFAULT_TILE_IMAGES);
			this.tileSize.setText(""+DEFAULT_TILE_SIZE);
			this.tileSize.setEnabled(false);
		}
		
		this.clusteringMethodCombo.select(0);
		this.legacyClusteringTypeCombo.select(0);
		this.clusterDistFraction.setText(""+DEFAULT_CLUSTER_DIST_FRACTION);
		this.clusterDistFractionCombo.select(2);
		setSimpleComboValue(clusterDistFractionCombo, clusterDistFraction);

		updateUi();
	}
	
	@Override
	public String getSelectedModelInfoString() {
//		TrpModelMetadata model = modelChooserBtn==null ? resolveSelectedNetFromParameters() : modelChooserBtn.getModel();
		TrpModelMetadata model = resolveSelectedNetFromParameters();
		return NEURAL_NET_LBL + " " + (model == null ? getDefaultModel().getName() : model.getName());
	}
	
	@Override
	public String constructConfigInfoString() {
		String infoStr = "";
		infoStr += getSelectedModelInfoString();
		
		infoStr += "\n\nBaseline detection Parameters:\n";
		infoStr += "  Min-Baseline-Length = "+parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"min_path_length", DEFAULT_MIN_PATH_LENGTH) + "\n";
		infoStr += "  Baseline-Accuracy-Threshold = "+ parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"bin_thresh", DEFAULT_BIN_THRESH) + "\n";
		if (!isNextGen()) {
			infoStr += "  Separator-Threshold = "+ parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"sep_thresh", DEFAULT_SEP_THRESH) + "\n";
		}
		infoStr += "  Max-Merge-Distance = "+ parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"max_dist_fraction", DEFAULT_MAX_DIST_FRACTION) + "\n";
		if (!isNextGen()) {
			infoStr += "  Image scaling = "+ parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"scale", DEFAULT_SCALING) + "\n";
		}
		if (isNextGen()) {
			infoStr += "  Image size = "+ parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"size", DEFAULT_SIZE) + "\n";
			if (parameters.containsKey(JobConst.PROP_PARS_PREFIX+"tile_images")) {
				infoStr += "  Tile images = "+ parameters.containsKey(JobConst.PROP_PARS_PREFIX+"tile_images") + "\n";
				infoStr += "  Tile size = "+ parameters.getIntParam(JobConst.PROP_PARS_PREFIX+"tile_size", DEFAULT_TILE_SIZE) + "\n";
			}
		}
		infoStr += "\nRegion detection Parameters:\n";
		String clusteringMethod = parameters.getParameterValue(JobConst.PROP_PARS_PREFIX+"clustering_method", "legacy");
		infoStr += "  Method = "+(clusteringMethod.equals("legacy") ? "General" : "Custom")+"\n";
		if (StringUtils.equalsIgnoreCase(clusteringMethod, "legacy")) {
			String legacyClusteringType = parameters.getParameterValue(JobConst.PROP_PARS_PREFIX+"clustering_legacy_type", "default");
			if (legacyClusteringType.equals("hom")) {
				legacyClusteringType = "Mixed";
			}
			else {
				legacyClusteringType = "Horizontal";
			}
			infoStr += "  Baseline orientation = "+ StringUtils.capitalize(legacyClusteringType) + "\n";	
		}
		else {
			infoStr += "  Max-Cluster-Distance = "+ parameters.getDoubleParam(JobConst.PROP_PARS_PREFIX+"cluster_dist_fraction", DEFAULT_CLUSTER_DIST_FRACTION) + "\n";	
		}
		
		return infoStr;
	}
	
	/**
	 * Checks the current parameter map for the LA model's filename and finds it in the modelList
	 * @return the resolved LaModel
	 */
	private TrpModelMetadata resolveSelectedNetFromParameters() {
		try {
			Integer modelId = parameters.getIntParam(JobConst.PROP_MODEL_ID);
			String name = parameters.getParameterValue(JobConst.PROP_MODELNAME);
			if (modelId == null || name == null) {
				return getDefaultModel();
			}
			TrpModelMetadata md = new TrpModelMetadata();
			md.setType(TrpHtr.TYPE_LAYOUT);
			md.setModelId(modelId);
			md.setName(name);
			md = TrpMainWidget.i().getStorage().getModelDetails(md);

			return md;		
		}
		catch (Exception e) {
			logger.warn("Could not resolveSelectedNetFromParameters: "+e.getMessage()+" - returning default model!");
			return getDefaultModel();
		}
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Layout Analysis Configuration");
//		newShell.setMinimumSize(480, 480);
	}

	@Override
	protected Point getInitialSize() {
		return SWTUtil.getPreferredOrMinSize(getShell(), 480, 350);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.HELP); //SWT.MAX | SWT.RESIZE
	}

	
	/* 
	 * Add help button to buttonBar
	 * 
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	  // Change parent layout data to fill the whole bar
//	  parent.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
	  defaultsButton = createButton(parent, IDialogConstants.NO_ID, "Apply defaults", false);
	  SWTUtil.onSelectionEvent(defaultsButton, e -> {
		  setDefaultValues();
	  });

	  helpButton = createButton(parent, IDialogConstants.HELP_ID, "Help", false);
	  helpButton.setImage(Images.HELP); //super.getImage(Dialog.DLG_IMG_HELP));
	  
	  createButton(parent, IDialogConstants.OK_ID, "OK", true);
	  createButton(parent, IDialogConstants.CANCEL_ID, "Cancel" , false);
	  GridData buttonLd = (GridData)getButton(IDialogConstants.CANCEL_ID).getLayoutData();
	  helpButton.setLayoutData(buttonLd);
	  helpButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DesktopUtil.browse(HELP_WIKI_PAGE, "You can find the relevant information on the Transkribus Wiki.", getParentShell());
			}
		});		
	  

	}
}
