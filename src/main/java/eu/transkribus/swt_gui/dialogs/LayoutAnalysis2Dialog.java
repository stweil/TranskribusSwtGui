package eu.transkribus.swt_gui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector.DocSelectorData;

public class LayoutAnalysis2Dialog extends Dialog {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LayoutAnalysis2Dialog.class);
	
	CurrentTranscriptOrDocPagesOrCollectionSelector dps;
	Combo modelCombo;
	Button typeLABtn;
	Button typeTableBtn;
	Button keepExistingRegionsBtn;
	
	int docId;
	String modelName;
	DocSelectorData docSelectorData;
	boolean keepExistingRegions=false;

	public LayoutAnalysis2Dialog(Shell parentShell) {
		super(parentShell);
	}
	
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Layout Analysis 2");
    }
	
	@Override
	protected Point getInitialSize() {
		Point s = SWTUtil.getPreferredOrMinSize(getShell(), 300, 0);
		return new Point(Math.max(300, s.x+30), s.y+15);
	}
	
	@Override
	protected void setShellStyle(int newShellStyle) {           
	    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.MIN | SWT.RESIZE);
	    setBlockOnOpen(false);
	}
	
	@Override
	protected boolean isResizable() {
	    return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		cont.setLayout(new GridLayout(2, false));
		
		dps = new CurrentTranscriptOrDocPagesOrCollectionSelector(cont, SWT.NONE, false, true, true);
		dps.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		typeLABtn = new Button(cont, SWT.RADIO);
		typeLABtn.setText("Layout-Analysis");
		SWTUtil.onSelectionEvent(typeLABtn, e -> updateUi());
		typeLABtn.setSelection(true);
		
		typeTableBtn = new Button(cont, SWT.RADIO);
		typeTableBtn.setText("Table-Detection");
		SWTUtil.onSelectionEvent(typeTableBtn, e -> updateUi());
		
		Label modelLbl = new Label(cont, 0);
		modelLbl.setText("Model: ");
		modelCombo = new Combo(cont, SWT.DROP_DOWN /*| SWT.READ_ONLY*/);
		modelCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		keepExistingRegionsBtn = new Button(cont, SWT.CHECK);
		keepExistingRegionsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		keepExistingRegionsBtn.setText("Keep existing regions");
		keepExistingRegionsBtn.setToolTipText("Append detected regions to current PAGE-XML");
		
		updateUi();
		
		return cont;
	}
	
	private void updateUi() {
		if (typeLABtn.getSelection()) {
			modelCombo.setItems("Berlin101", "Darmstadt", "Koeln", "Stabz_101", "gradsko_101");
		}
		else {
			modelCombo.setItems("Amsterdam_Demo");
		}
		modelCombo.select(0);
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
		
	    Button runBtn = createButton(parent, IDialogConstants.OK_ID, "Run...", false);
	    runBtn.setImage(Images.ARROW_RIGHT);
	}
	
	public String getSelectedModel() {
		return modelCombo.getText();
	}
	
	public int getSelectedModelIndex() {
		return modelCombo.getSelectionIndex();
	}
	
	@Override
	protected void okPressed() {
		this.modelName = modelCombo.getText();
		this.docSelectorData = dps.getData();
		if (this.docSelectorData == null) {
			return;
		}
		this.keepExistingRegions=keepExistingRegionsBtn.getSelection();
		
		super.okPressed();
	}

	public CurrentTranscriptOrDocPagesOrCollectionSelector getDps() {
		return dps;
	}

	public DocSelectorData getDocSelectorData() {
		return docSelectorData;
	}

	public String getModelName() {
		return modelName;
	}	
	
	public boolean isKeepExistingRegions() {
		return keepExistingRegions;
	}
	
}
