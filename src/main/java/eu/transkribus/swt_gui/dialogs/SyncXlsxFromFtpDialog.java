package eu.transkribus.swt_gui.dialogs;

import java.awt.Desktop;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.ServerErrorException;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.client.util.FtpConsts;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.exceptions.NoConnectionException;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpDocDir;
import eu.transkribus.swt.mytableviewer.ColumnConfig;
import eu.transkribus.swt.mytableviewer.MyTableViewer;
import eu.transkribus.swt.util.DefaultTableColumnViewerSorter;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.mainwidget.storage.IStorageListener;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.upload.DocDirTableLabelProvider;


public class SyncXlsxFromFtpDialog extends Dialog {
	private final static Logger logger = LoggerFactory.getLogger(SyncXlsxFromFtpDialog.class);
	
	private final static String ENC_USERNAME = Storage.getInstance().getUser().getUserName().replace("@", "%40");
	
	private final static String INFO_MSG = 
			"You can upload xlsx files to:\n\n"
			+ FtpConsts.FTP_PROT + FtpConsts.FTP_URL + "\n\n"
			+ "by using your favorite FTP client.\n"
			+ "For accessing the FTP server please use your\n"
			+ "Transkribus credentials.\n"
			+ "After the upload is done, you can sync the\n"
			+ "chosen file with the current document\n";
	
	
	String dirName;
	Link link;
	private Table filenameTable;
	private MyTableViewer filenameTv;
	List<TrpDocDir> fileDirs = new ArrayList<>(0);
	String title;

	private List<TrpDocDir> selFiles;

	Button reloadBtn;
	Button helpBtn;

	Storage store = Storage.getInstance();
	TrpMainWidget mw = TrpMainWidget.getInstance();

	public static final String DIRECTORY_COL = "Directory";
	public static final String TITLE_COL = "Title";
	public static final String NR_OF_IMGS_COL = "Nr. of Images";
	public static final String SIZE_COL = "Size";
	public static final String CREATE_DATE_COL = "Last modified";
	
	public static final ColumnConfig[] DOC_DIR_COLS = new ColumnConfig[] {
		new ColumnConfig(DIRECTORY_COL, 180, true, DefaultTableColumnViewerSorter.ASC),
		new ColumnConfig(TITLE_COL, 110, false, DefaultTableColumnViewerSorter.ASC),
		new ColumnConfig(NR_OF_IMGS_COL, 110, false, DefaultTableColumnViewerSorter.ASC),
		new ColumnConfig(SIZE_COL, 100, false, DefaultTableColumnViewerSorter.ASC),
		new ColumnConfig(CREATE_DATE_COL, 150, false, DefaultTableColumnViewerSorter.ASC),
	};
	
	public SyncXlsxFromFtpDialog(Shell parentShell) {
		super(parentShell);
		
		setShellStyle(SWT.SHELL_TRIM | SWT.MODELESS | SWT.BORDER | SWT.TITLE);
	}
	
	@Override protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout)container.getLayout();
		gridLayout.numColumns = 3;
//		GridData gridData = (GridData)container.getLayoutData();
//		gridData.widthHint = 600;
//		gridData.heightHint = 500;
//		gridData.minimumWidth = 600;
//		gridData.minimumHeight = 500;
//		container.setSize(700, 600);

		
		Label lblDir = new Label(container, SWT.NONE);
		lblDir.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDir.setText("Location:");
		link = new Link(container, SWT.NONE);
	    final String linkText = "<a href=\"" + FtpConsts.FTP_PROT + ENC_USERNAME + "@" + FtpConsts.FTP_URL + "\">"+ FtpConsts.FTP_PROT + FtpConsts.FTP_URL + "</a>";
	    link.setText(linkText);
	    helpBtn = new Button(container, SWT.NONE);
		helpBtn.setImage(Images.getOrLoad("/icons/help.png"));
		helpBtn.setToolTipText("What's that?");
		
		filenameTv = new MyTableViewer(container, SWT.MULTI | SWT.FULL_SELECTION);
		filenameTv.setContentProvider(new ArrayContentProvider());
		filenameTv.setLabelProvider(new DocDirTableLabelProvider(filenameTv));
		
		filenameTable = filenameTv.getTable();
		filenameTable.setHeaderVisible(true);
		filenameTable.setLinesVisible(true);
		filenameTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		
		filenameTv.addColumns(DOC_DIR_COLS);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		reloadBtn = new Button(container, SWT.NONE);
		reloadBtn.setImage(Images.REFRESH);
		reloadBtn.setToolTipText("Reload the files from the FTP server");
						
		updateDocDirs();
		addListener();
		return container;
	}
		
	private void addListener() {
		
		link.addSelectionListener(new SelectionAdapter(){
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	        	Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	                try {
	                    desktop.browse(new URI(e.text));
	                } catch (Exception ex) {
	                	//UnsupportedOperationException - if the current platform does not support the Desktop.Action.BROWSE action
	                	//IOException - if the user default browser is not found, or it fails to be launched, or the default handler application failed to be launched
	                	//SecurityException - if a security manager exists and it denies the AWTPermission("showWindowWithoutWarningBanner") permission, or the calling thread is not allowed to create a subprocess; and not invoked from within an applet or Java Web Started application
	                	//IllegalArgumentException - if the necessary permissions are not available and the URI can not be converted to a URL
	                	logger.error("Could not open ftp client!");
	                	
	                	DialogUtil.showMessageBox(getShell(), "Could not find FTP client", INFO_MSG, SWT.NONE);
	                }
	            }
	        	
//	        	try {
//	        		//  Open default external browser 
//	        		PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(new URL(e.text));
//	        	} catch (PartInitException ex) {
//	        		// TODO Auto-generated catch block
//	        		ex.printStackTrace();
//	            } catch (MalformedURLException ex) {
//	            	// TODO Auto-generated catch block
//	            	ex.printStackTrace();
//	            }
	        }
	    });
				
		reloadBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				updateDocDirs();
			}
		});
		
		helpBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				DialogUtil.showInfoMessageBox(getParentShell(), "Information", INFO_MSG);
			}
		});
	}
	
	@Override protected Control createButtonBar(Composite parent) {
		Control ctrl = super.createButtonBar(parent);
		
		return ctrl;
	}
		
	
	private void updateDocDirs(){
		try {
			fileDirs = store.listFilesOnFtp();
		} catch (SessionExpiredException | ServerErrorException | IllegalArgumentException
				| NoConnectionException e) {
			mw.onError("Error", "Could not load directory list!", e);
		}
		filenameTv.setInput(fileDirs);
	}
	

	// overriding this methods allows you to set the
	// title of the custom dialog
	@Override protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Sync from FTP Server");
	}

	@Override protected Point getInitialSize() {
		return new Point(700, 500);
	}

	// override method to use "Login" as label for the OK button
	@Override protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Sync back", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override protected void okPressed() {
		saveInput();
		if(selFiles == null || selFiles.isEmpty()){
			DialogUtil.showErrorMessageBox(getParentShell(), "Info", "You have to select a file for ingesting.");
		} else {
			super.okPressed();
		}
	}

	private void saveInput() {
		this.selFiles = getSelectedDocDirs();
	}
	
	public List<TrpDocDir> getSelectedFiles(){
		return selFiles;
	}
		
	public List<TrpDocDir> getSelectedDocDirs(){
		IStructuredSelection sel = (IStructuredSelection) filenameTv.getSelection();
		List<TrpDocDir> list = new LinkedList<>();
		Iterator<Object> it = sel.iterator();
		while(it.hasNext()){
			final TrpDocDir docDir = (TrpDocDir)it.next();
			logger.debug("Selected dir: " + docDir.getName());
			list.add(docDir);
		}
		return list;	
	}
	
	public static void main(String[] args) {
		ApplicationWindow aw = new ApplicationWindow(null) {
			@Override
			protected Control createContents(Composite parent) {
				// getShell().setLayout(new FillLayout());
				getShell().setSize(300, 200);

				Button btn = new Button(parent, SWT.PUSH);
				btn.setText("Open upload dialog");
				btn.addSelectionListener(new SelectionListener() {

					@Override public void widgetSelected(SelectionEvent e) {
						(new SyncXlsxFromFtpDialog(getShell())).open();
					}

					@Override public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				SWTUtil.centerShell(getShell());

				return parent;
			}
		};
		aw.setBlockOnOpen(true);
		aw.open();

		Display.getCurrent().dispose();
	}
}
