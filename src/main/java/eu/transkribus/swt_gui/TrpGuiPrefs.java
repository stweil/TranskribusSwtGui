package eu.transkribus.swt_gui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.transkribus.client.connection.TrpServer;
import eu.transkribus.core.model.beans.auth.TrpUser;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.util.JacksonUtil;
import eu.transkribus.core.util.ProxyUtils.ProxySettings;
import eu.transkribus.swt_gui.auth.LoginDialog;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * In linux prefs are stored in: (by default - can be changed by setting java.util.prefs.userRoot property!)
 * 	${user.home}/.java/.userPrefs/org/dea/transcript/trp/gui/creds
 * In windows in the registry under:
 * 	HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft\Prefs and HKEY_CURRENT_USER\Software\JavaSoft\Prefs\org\dea\transcript\trp\gui\creds
 * 
 * @author sebastianc
 */
public class TrpGuiPrefs {
	private final static Logger logger = LoggerFactory.getLogger(TrpGuiPrefs.class);
	
//	final static String wtf = "PMpCIaf8HUUjbNMW1DvpmERZqov9ZMQaE6e7SFypaFg=";
	final static String wtf = "PMpCIaf8HUUjbNMW1DvpmE";
	
	public static final String ACCOUNT_NODE = "acc";
	public static final String CREDS_NODE = "creds";
	public static final String EXPORT_NODE = "export";
	public static final String PROXY_NODE = "proxy";
	public static final String LA_NODE = "LA";
	
	@Deprecated
	public static final String OAUTH_NODE = "oauth";
	@Deprecated
	private static final String OAUTH_UN_KEY = "_un";
	@Deprecated
	private static final String OAUTH_PIC_KEY = "_pic";

	public static final String SSO_NODE = "sso";

	private static final String LAST_ACCOUNT_TYPE_KEY = "accType";
	public static final String LAST_USER_KEY = "lastUser";
	public static final String UN_KEY = "trpUn";
	public static final String PW_KEY = "trpPw";
	
	public static final String LAST_EXPORT_FOLDER_KEY = "lastExportFolder";
	
	public static final String PROXY_ENABLED_KEY = "enabled";
	public static final String PROXY_HOST_KEY = "host";
	public static final String PROXY_PORT_KEY = "port";
	public static final String PROXY_USER_KEY = "user";
	public static final String PROXY_PASS_KEY = "password";
	
	
	static Preferences pref = Preferences.userNodeForPackage(TrpGui.class).node(CREDS_NODE);
	static Preferences oAuthPrefs = Preferences.userNodeForPackage(TrpGui.class).node(OAUTH_NODE);
	static Preferences ssoPrefs = Preferences.userNodeForPackage(TrpGui.class).node(SSO_NODE);
	static Preferences accountPrefs = Preferences.userNodeForPackage(TrpGui.class).node(ACCOUNT_NODE);
	
	static Preferences exportPrefs = Preferences.userNodeForPackage(TrpGui.class).node(EXPORT_NODE);

	static Preferences proxyPrefs = Preferences.userNodeForPackage(TrpGui.class).node(PROXY_NODE);
	static Preferences laPrefs = Preferences.userNodeForPackage(TrpGui.class).node(LA_NODE);

	public static String encryptAes(String key, String strToEncrypt) {
		String encryptedString;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			SecretKeySpec secretKey = new SecretKeySpec(Base64.decodeBase64(key), "AES");

			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				 | BadPaddingException e) {
			throw new RuntimeException(e);
		}
		return encryptedString;
	}
	
	public static byte[] generateAesKey() throws NoSuchAlgorithmException {
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(256); // for example
		SecretKey secretKey = keyGen.generateKey();
		
		return secretKey.getEncoded();
	}
	
	public static String decryptAes(String key, String strToDecrypt) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(Base64.decodeBase64(key), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
		return decryptedString;
	}
	
	public static List<String> getUsers() {
		List<String> users = new ArrayList<>();
		try {
			for (String k : pref.keys()) {
				if (!k.equals(CREDS_NODE)) {
					users.add(k);
				}
			}
		} catch (BackingStoreException e) {
			logger.error(e.getMessage());
		}
		return users;
	}
	
	public static void clearCredentials() throws Exception {
		pref.clear();
	}
	
	public static void clearCredentials(String username) throws Exception {
		pref.remove(username);
	}
	
	public static void storeCredentials(String username, String pw) {
		logger.debug("storing credentials for user: "+username);
		
		pref.put(username, encryptAes(wtf, pw));
	}
	
	public static void storeLastExportFolder(String foldername){
		logger.debug("storing export folder "+foldername);		
		exportPrefs.put(LAST_EXPORT_FOLDER_KEY, foldername);
	}
	
	public static String getLastExportFolder() throws Exception {
	
		return exportPrefs.get(LAST_EXPORT_FOLDER_KEY, null);
		
	}
	
	public static void storeLastLogin(String username) {
		pref.put(LAST_USER_KEY, username);
	}

	/**
	 * @return the most recent username used for logging in, or null if there is none.
	 */
	public static String getLastLogin() {
		return pref.get(LAST_USER_KEY, null);
	}

	public static Pair<String, String> getLastStoredCredentials() {
		return getStoredCredentials(null);
	}
	
	/**
	 * Retrieves stored credentials for the given username if it exists, returns null elsewise.
	 * If the parameter username is null it will return the user stored by the storeLastLogin call.
	 */
	public static Pair<String, String> getStoredCredentials(String username) {
		try {
			if (username == null) {
				username = getLastLogin();
				if (username == null) // no "last user" stored
					return null;
			}
			
			String pwEnc = pref.get(username, null);
			if (pwEnc == null) { // no credentials stored for this user
				return null;
			}
			return Pair.of(username, decryptAes(wtf, pwEnc));
		} catch (Exception e) {
			logger.error("Could not retrieve stored creds: "+e.getMessage());
			return null;
		}
	}

	public static void storeLastAccountType(String accType) {
		accountPrefs.put(LAST_ACCOUNT_TYPE_KEY, accType);
	}

	public static String getLastLoginAccountType() {
		return accountPrefs.get(LAST_ACCOUNT_TYPE_KEY, LoginDialog.TRANSKRIBUS_ACCOUNT_TYPE);
	}

	private static Preferences getSsoPrefsNode(String authServerUrl) {
		if(authServerUrl == null) {
			authServerUrl = TrpServer.Prod.getAuthServerUriStr();
		}
		final String authServerId = authServerUrl.replaceAll("[./:-]", "");
		logger.debug("SSO prefs node for auth server '{}': {}", authServerUrl, authServerId);
		Preferences ssoAuthServerNode = ssoPrefs.node(authServerId);
		if(authServerUrl.equals(TrpServer.Prod.getAuthServerUriStr())) {
			findAndMigrateLegacyEntries(ssoAuthServerNode);
		}
		return ssoAuthServerNode;
	}

	public static void storeSsoToken(final String authServerUrl, final TrpUser user, final String refreshToken) {
		logger.debug("Storing sso token for user {}", user.getUserName());
		ObjectMapper mapper = JacksonUtil.createDefaultMapper(false);
		//find any entry with matching userId and replace it.
		for(String username : getSsoUsernames(authServerUrl)) {
			try {
				final SsoToken ssoToken = mapper.readValue(getSsoTokenString(authServerUrl, username), SsoToken.class);
				if(ssoToken.getUserId()!=null && ssoToken.getUserId() != 0 && ssoToken.getUserId() == user.getUserId()
					&& !ssoToken.getUsername().equals(user.getUserName())) {
					logger.debug("Found stored token with matching user ID but username mismatch. Removing it.");
					clearSsoToken(authServerUrl, ssoToken.getUsername());
				}
			} catch (JsonProcessingException e) {
				logger.error("Could not decrypt stored token for user: {}", user.getUserName());
			}
		}
		SsoToken encodedToken = new SsoToken(user, encryptAes(wtf, refreshToken));
		try {
			getSsoPrefsNode(authServerUrl).put(user.getUserName(), mapper.writeValueAsString(encodedToken));
		} catch (JsonProcessingException e) {
			logger.error("Could not serialize refresh token for storage.", e);
//			throw new IllegalStateException("Could not serialize refresh token for storage.", e);
		};
	}

	/**
	 * Retrieves stored refreshToken for the given username if it exists, returns null otherwise.
	 */
	public static String getRefreshToken(final String authServerUrl, final String username) {
		String ssoTokenString = getSsoTokenString(authServerUrl, username);
		if(ssoTokenString == null) {
			return null;
		}
		try {
			SsoToken ssoToken = JacksonUtil.createDefaultMapper(false)
					.readValue(ssoTokenString, SsoToken.class);
			return decryptAes(wtf, ssoToken.getRefreshToken());
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
				| NoSuchPaddingException | JsonProcessingException e) {
			logger.error("Could not decrypt stored refresh token for user: {}", username, e);
			return null;
		}
	}

	private static String getSsoTokenString(final String authServerUrl, final String username) {
		if (username == null) {
			return null;
		}

		String tokenEnc = getSsoPrefsNode(authServerUrl).get(username, null);
		if (tokenEnc == null) { // no credentials stored for this user
			logger.debug("Did not find token for username: {}", username);
			return null;
		}
		logger.debug("Found sso token for user: {}", username);
		return tokenEnc;
	}

	public static List<String> getSsoUsernames(final String authServerUrl) {
		String[] users = {};
		try {
			users = getSsoPrefsNode(authServerUrl).keys();
			logger.debug("SSO prefs keys: {}", Arrays.asList(ssoPrefs.keys()));
		} catch (BackingStoreException e) {
			logger.error("Could not access sso token storage.", e);
			return Collections.EMPTY_LIST;
		}
		logger.debug("Found stored SSO usernames: {}", users);
		return Arrays.asList(users);
	}

	public static void clearSsoToken(final String authServerUrl, final String username) {
		getSsoPrefsNode(authServerUrl).remove(username);
	}

	/**
	 * Migrate old data stored by TX < 1.26.0 to an authServer node
	 */
	private static void findAndMigrateLegacyEntries(Preferences ssoAuthServerNode) {
		try {
			if(ssoPrefs.keys().length == 0) {
				return; //nothing to do
			}
			ObjectMapper mapper = JacksonUtil.createDefaultMapper(false);
			for(String key : ssoPrefs.keys()) {
				final String username = extractSsoUsernameFromKey(key);
				final String encryptedToken = ssoPrefs.get(key, null);
				SsoToken ssoToken = new SsoToken();
				ssoToken.setUsername(username);
				ssoToken.setRefreshToken(encryptedToken);
				try {
					ssoAuthServerNode.put(username, mapper.writeValueAsString(ssoToken));
				} catch (JsonProcessingException e) {
					logger.debug("Failed to migrate a store refresh token. Removing it...");
				}
				ssoPrefs.remove(key);
			}
		} catch (BackingStoreException e) {
			logger.warn("Failed to retrieve legacy SSO preferences entries for migration.", e);
		}
	}

	@Deprecated
	private static String extractSsoUsernameFromKey(final String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return StringUtils.removeStart(key, "user.");
	}

	/**
	 * Retrieves stored refreshToken for the given OauthProvider if it exists, returns null elsewise.
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidKeyException
	 */
	@Deprecated
	public static OAuthCreds getOAuthCreds() {
		final String prov = "Google";
		String tokenEnc = oAuthPrefs.get(prov.toString(), null);
		if (tokenEnc == null) { // no credentials stored for this provider
			logger.debug("Did not find token for OAuth Provider: "+ prov);
			return null;
		} else {
			logger.debug("Found token for OAuth Provider: "+ prov);
		}
		String tokenClear;
		try {
			tokenClear = decryptAes(wtf, tokenEnc);
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
				 | NoSuchPaddingException e) {
			logger.error("Could not decrypt token for OAuthProvider: " + prov, e);
			return null;
		}

		final String userName = oAuthPrefs.get(prov.toString() + OAUTH_UN_KEY, null);
		final String profilePicUrl = oAuthPrefs.get(prov.toString() + OAUTH_PIC_KEY, null);

		OAuthCreds creds = new OAuthCreds();
		creds.setProfilePicUrl(profilePicUrl);
		creds.setUserName(userName);
		creds.setRefreshToken(tokenClear);

		return creds;
	}
	
	public static void clearOAuthToken() {
		final String provider = "Google";
		oAuthPrefs.remove(provider);
		oAuthPrefs.remove(provider + OAUTH_UN_KEY);
		oAuthPrefs.remove(provider + OAUTH_PIC_KEY);
	}
	
	public static void setProxyPrefs (ProxyPrefs p) {
		if(p == null) {
			throw new IllegalArgumentException("ProxyPrefs object is null!");
		}
		proxyPrefs.putBoolean(PROXY_ENABLED_KEY, p.isEnabled());
		proxyPrefs.put(PROXY_HOST_KEY, p.getHost());
		proxyPrefs.putInt(PROXY_PORT_KEY, p.getPort());
		proxyPrefs.put(PROXY_USER_KEY, p.getUser());
		
		if(p.getPassword().isEmpty()) {
			proxyPrefs.put(PROXY_PASS_KEY, "");
		} else {
			String encrPw;
			try {
				encrPw = encryptAes(wtf, p.getPassword());
			} catch (RuntimeException e) {
				logger.error("Could not encrypt proxy password!", e);
				encrPw = "";
			}
			proxyPrefs.put(PROXY_PASS_KEY, encrPw);
		}
	}
	
	public static ProxyPrefs getProxyPrefs () {
		ProxyPrefs p = new ProxyPrefs();
		p.setEnabled(proxyPrefs.getBoolean(PROXY_ENABLED_KEY, false));
		p.setHost(proxyPrefs.get(PROXY_HOST_KEY, ""));
		p.setPort(proxyPrefs.getInt(PROXY_PORT_KEY, -1));
		p.setUser(proxyPrefs.get(PROXY_USER_KEY, ""));
		final String encrPw = proxyPrefs.get(PROXY_PASS_KEY, "");
		if(encrPw.isEmpty()) {
			p.setPassword("");
		} else {
			try {
				p.setPassword(decryptAes(wtf, encrPw));
			} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException e) {
				logger.error("Could not decrypt proxy password!", e);
				p.setPassword("");
			}
		}
		return p;
	}
	
	/**
	 * Returns the sub-node-name depending on the currently logged-in server - localhost if not logged-in
	 */
	private static String getServerNodeName() {
		String currentServer = Storage.i().getCurrentServer();
		if (currentServer == null) {
			return "localhost";
		}
		try {
			return currentServer.substring(currentServer.lastIndexOf("/")+1);
		} catch (Exception e) {
			logger.error("Could not parse server name: "+currentServer);
			return "NA";
		}
	}

	public static ParameterMap getLaParameters(JobImpl impl) {
		ParameterMap map = new ParameterMap();
		if(impl == null) {
			return map;
		}
		//check if this is an LA job and normalize impl to base impl name if necessary
		impl = JobImpl.getBaseLaJob(impl);
		logger.debug("server-node-name = "+getServerNodeName());
		Preferences implPrefs = laPrefs.node(impl.toString()).node(getServerNodeName());
		try {
			for(String key : implPrefs.keys()) {
				final String value = implPrefs.get(key, null);
				if(value != null) {
					map.addParameter(key, value);
				}
			}
			logger.debug("Loaded stored parameters for LA: " + impl + "\n" + map.toString());
		} catch (BackingStoreException e) {
			logger.error("Could not retrieve preferences for " + impl.toString() + "!", e);
		}
		return map;
	}
	
	public static void storeLaParameters(JobImpl impl, ParameterMap params) {
		if(impl == null) {
			return;
		}
		if(params == null) {
			return;
		}
		//check if this is an LA job and normalize impl to base impl name if necessary
		impl = JobImpl.getBaseLaJob(impl);
		Preferences implPrefs = laPrefs.node(impl.toString()).node(getServerNodeName());
		try {
			implPrefs.clear();
		} catch (BackingStoreException e) {
			logger.error("Could not clear config for " + impl + " LA!", e);
		}
		for(Entry<String, String> e : params.getParamMap().entrySet()) {
			if(e.getKey() != null && e.getValue() != null) {
				implPrefs.put(e.getKey(), e.getValue());
			}
		}
		try {
			implPrefs.flush();
			logger.debug("Stored parameters for LA: " + impl + "\n" + params.toString());
		} catch (BackingStoreException e) {
			logger.error("Could not persist config for " + impl + " LA!", e);
		}
	}
	
	public static void testPreferences() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		
//		byte[] key = generateAesKey();
//		System.out.println(Base64.encodeBase64String(key));
		
		
		String key = "whatever";
//		pref.put(key, encryptAes(rawKey, "test"));
		
		String value = pref.get(key, "");
		System.out.println("value encyrpted: "+value);
		System.out.println("value decrypted: "+decryptAes(wtf, value));
		
//		Preferences sp = new Preferences(TrpGui.class, rawKey, true);
//		
//		
//		
//		sp.put(key, "blabla");
//		
//		String value = sp.getString(key, "default");
//		System.out.println("value of "+key+": "+value);
	}
	
	public static void main(String[] args) throws Exception {
		testPreferences();
	}

	@Deprecated
	public static class OAuthCreds {
		private String refreshToken;
		private String userName;
		private String profilePicUrl;
		public OAuthCreds(){
		}
		public String getProvider(){
			return "Google";
		}
		public String getRefreshToken() {
			return refreshToken;
		}
		public void setRefreshToken(String refreshToken) {
			this.refreshToken = refreshToken;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getProfilePicUrl() {
			return profilePicUrl;
		}
		public void setProfilePicUrl(String profilePicUrl) {
			this.profilePicUrl = profilePicUrl;
		}
		@Override
		public String toString() {
			return "OAuthCreds [refreshToken=" + refreshToken + ", userName=" + userName + ", profilePicUrl="
					+ profilePicUrl + "]";
		}
	}

	public static class SsoToken {
		public SsoToken() {}
		public SsoToken(TrpUser user, String encodedToken) {
			this.userId = user.getUserId() == 0 ? null : user.getUserId();
			this.email = user.getEmail();
			this.username = user.getUserName();
			this.refreshToken = encodedToken;
		}
		private String refreshToken;
		private String username;
		private String email;
		private Integer userId;
		public String getRefreshToken() {
			return refreshToken;
		}
		public void setRefreshToken(String refreshToken) {
			this.refreshToken = refreshToken;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Integer getUserId() {
			return userId;
		}
		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		@Override
		public String toString() {
			return "SsoToken{" +
					"refreshToken='" + refreshToken + '\'' +
					", username='" + username + '\'' +
					", email='" + email + '\'' +
					", userId=" + userId +
					'}';
		}
	}

	public static class ProxyPrefs extends ProxySettings {
		private boolean enabled;
		
		public ProxyPrefs() {
			super();
			enabled = false;
		}
		public ProxyPrefs(boolean enabled, String host, int port, String user, String password) {
			super(host, port, user, password);
			this.enabled = enabled;
		}
		public boolean isEnabled() {
			return enabled;
		}
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
		@Override
		public String toString() {
			return "ProxyPrefs [enabled=" + enabled + ", host=" + host + ", port=" + port + ", user=" + user
					+ ", password=" + password + "]";
		}
	}
}
