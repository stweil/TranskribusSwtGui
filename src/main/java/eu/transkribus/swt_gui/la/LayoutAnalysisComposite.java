package eu.transkribus.swt_gui.la;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.DocSelection;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.swt.util.LabeledCombo;
import eu.transkribus.swt.util.LabeledComboWithButton;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.TrpGuiPrefs;
import eu.transkribus.swt_gui.dialogs.ALaConfigDialog;
import eu.transkribus.swt_gui.dialogs.CITlabAdvancedLaConfigDialog;
import eu.transkribus.swt_gui.dialogs.TranskribusLaConfigDialog;
import eu.transkribus.swt_gui.htr.StructureTagComposite;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.mainwidget.storage.IStorageListener;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;

public class LayoutAnalysisComposite extends Composite {
	private static final Logger logger = LoggerFactory.getLogger(LayoutAnalysisComposite.class);
	
	public static boolean TEST = false;
	public static final boolean IS_CONFIGURABLE = true;
	
	static private Storage store = TEST ? null : Storage.getInstance();
	private CurrentTranscriptOrDocPagesOrCollectionSelector dps;
	private Button doBlockSegBtn, doLineSegBtn;
	private Button doWordSegBtn;
	private Button enrichOldTranscriptBtn;
	private Button splitOnRegionBodersBtn;
	private LabeledText overlapFractionText;
	private Label selectedModelLabel;
	private LabeledCombo methodCombo;
	
	public static final String METHOD_TRANSKRIBUS = "Transkribus LA";
	public static final String METHOD_PRINTED_BLOCKS = "Printed Block Detection";
	public static final String METHOD_SEPARATORS = "Separator Detection";
	public static final String METHOD_KRAKEN = "Kraken";
	public static final String METHOD_TRANSKRIBUS_NEXTGEN = "Transkribus LA NextGen";
	
	private ParameterMap paramMap;
	private ALaConfigDialog configDialog = null;
	private String currentConfigString = null;
	
	private Composite mainContainer, tmpContainer;
	private StructureTagComposite structureTagComp;

	public LayoutAnalysisComposite(Composite parent, int style) {
		super(parent, style);
		
		mainContainer = (Composite) this;
		GridLayout gl = new GridLayout(1, false);
		gl.marginHeight = gl.marginWidth = 0;
		this.setLayout(gl);
		
		final String labelTxt = "Method: ";
		if(IS_CONFIGURABLE) {
			methodCombo = new LabeledComboWithButton(mainContainer,  labelTxt, "Configure...");
		} else {
			methodCombo = new LabeledCombo(mainContainer,  labelTxt);
		}
		methodCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		updateMethods();
//		methodCombo.combo.setItems(getMethods(true).toArray(new String[0]));
//		methodCombo.combo.select(0);
				
		//with this selector jobs can be started for complete collections
		dps = new CurrentTranscriptOrDocPagesOrCollectionSelector(mainContainer, SWT.NONE, true, true, true, true);		
		dps.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));

//		Group checkGrp = new Group(mainContainer,SWT.NONE);
//		checkGrp.setLayout(new GridLayout(1, false));
//		checkGrp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 2));
		
		tmpContainer = new Composite(mainContainer, SWT.FILL);
		tmpContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tmpContainer.setLayout(new GridLayout(3, false));
		
		doBlockSegBtn = new Button(tmpContainer, SWT.CHECK);
		doBlockSegBtn.setText("Find Text-Regions");
		doBlockSegBtn.setSelection(true);
		doBlockSegBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		SWTUtil.onSelectionEvent(doBlockSegBtn, e -> {
			updateOptionVisibility();
		});
		
		doLineSegBtn = new Button(tmpContainer, SWT.CHECK);
		doLineSegBtn.setText("Find Lines");
		doLineSegBtn.setToolTipText("Find Lines in Text-Regions");
		doLineSegBtn.setSelection(true);
		
		// not used currently...
		doWordSegBtn = new Button(SWTUtil.dummyShell, SWT.CHECK);
		doWordSegBtn.setText("Find Words");
		doWordSegBtn.setToolTipText("Find Words in Lines (experimental!)");
		doWordSegBtn.setVisible(false);
		
		enrichOldTranscriptBtn = new Button(tmpContainer, SWT.CHECK);
		enrichOldTranscriptBtn.setText("Enrich PAGE XML");
		enrichOldTranscriptBtn.setToolTipText("Enrich the current PAGE XML");
		enrichOldTranscriptBtn.setVisible(false);
		enrichOldTranscriptBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));		

		structureTagComp = new StructureTagComposite(tmpContainer);
		structureTagComp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		structureTagComp.setVisible(false);
		
		overlapFractionText = new LabeledText(tmpContainer, "Min line/region overlap fraction: ");
		overlapFractionText.setToolTipText("Minimum fraction of overlap between baseline and text-region");
		overlapFractionText.setText(""+0.1);
		overlapFractionText.setVisible(false);
		overlapFractionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		splitOnRegionBodersBtn = new Button(tmpContainer, SWT.CHECK);
		splitOnRegionBodersBtn.setText("Split lines on regions");
		splitOnRegionBodersBtn.setToolTipText("Split baselines on text-region borders");
		splitOnRegionBodersBtn.setVisible(false);
//		SWTUtil.onSelectionEvent(splitOnRegionBodersBtn, e -> {
//			overlapFractionText.setText(splitOnRegionBodersBtn.getSelection() ? "0.1" : "0.1");
//		});		
		
		selectedModelLabel = new Label(tmpContainer, 0);
		selectedModelLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		addListener();
		
		updateGui();
	}
	
	private void updateOptionVisibility() {
		boolean isUseRegions = !isDoBlockSeg() && isTranskribusLaSelected();
		structureTagComp.setVisible(isUseRegions);
		splitOnRegionBodersBtn.setVisible(isUseRegions);
		overlapFractionText.setVisible(isUseRegions);
	}

	private void updateMethods() {
		methodCombo.combo.setItems(getMethods(true).toArray(new String[0]));
		methodCombo.combo.select(0);
		//load stored params
		paramMap = TrpGuiPrefs.getLaParameters(getJobImpl());
	}

	private void updateSelectedModelInfoString() {
		if (isSelectedMethodConfigurable()) {
			selectedModelLabel.setText(getSelectedModelInfoString());
		}
		else {
			selectedModelLabel.setText("");
		}
	}	
	
	private void updateGui() {
		String method = getSelectedMethod();
			
		doBlockSegBtn.setEnabled(true);
		doLineSegBtn.setEnabled(true);
		doWordSegBtn.setEnabled(true);
		enrichOldTranscriptBtn.setVisible(false);
		
		switch(method) {
		case METHOD_TRANSKRIBUS:
		case METHOD_TRANSKRIBUS_NEXTGEN:
			doBlockSegBtn.setSelection(true);
			doBlockSegBtn.setEnabled(true);
			doLineSegBtn.setSelection(true);
			doLineSegBtn.setEnabled(true);
			doWordSegBtn.setSelection(false);
			doWordSegBtn.setEnabled(false);
			break;
		case METHOD_KRAKEN:
			doBlockSegBtn.setSelection(true);
			doBlockSegBtn.setEnabled(false);
			doLineSegBtn.setSelection(true);
			doLineSegBtn.setEnabled(false);
			doWordSegBtn.setSelection(false);
			doWordSegBtn.setEnabled(false);
			break;			
		case METHOD_PRINTED_BLOCKS:
			doBlockSegBtn.setSelection(true);
			doBlockSegBtn.setEnabled(false);
			doLineSegBtn.setSelection(false);
			doLineSegBtn.setEnabled(false);
			doWordSegBtn.setSelection(false);
			doWordSegBtn.setEnabled(false);
			break;
		case METHOD_SEPARATORS:
			doBlockSegBtn.setSelection(true);
			doBlockSegBtn.setEnabled(false);
			doLineSegBtn.setSelection(false);
			doLineSegBtn.setEnabled(false);
			doWordSegBtn.setSelection(false);
			doWordSegBtn.setEnabled(false);
			enrichOldTranscriptBtn.setVisible(true);
			break;

		default:
			return;
		}
		
		updateSelectedModelInfoString();
		
		//enable config button only if method is configurable (only CITlabAdvanced for now)
		if(methodCombo instanceof LabeledComboWithButton) {
			((LabeledComboWithButton)methodCombo).getButton().setEnabled(isSelectedMethodConfigurable());
		}
		
		updateOptionVisibility();
		updateCurrentConfigInfoString();
	}
	
	public static boolean isUserAllowedCitlab() {
		if (TEST || store.isAdminLoggedIn())
			return true;
		
		try {
			return store.isUserAllowedForJob(JobImpl.CITlabLaJob.toString(), false);
		} catch (Exception e) {
			logger.error("Could not determine if user is allowed for CITlabLaJob: "+e.getMessage());
			return false;
		}
	}

	public static List<String> getMethods(boolean withCustom) {
		List<String> methods = new ArrayList<>();

		methods.add(METHOD_TRANSKRIBUS);	
		methods.add(METHOD_KRAKEN);
		if (store.isAdminLoggedIn()) {
			methods.add(METHOD_TRANSKRIBUS_NEXTGEN);
		}
		
		methods.add(METHOD_PRINTED_BLOCKS);
		methods.add(METHOD_SEPARATORS);

		
		return methods;
	}
	
	private void addListener() {			
		methodCombo.combo.addModifyListener(new ModifyListener() {
			@Override public void modifyText(ModifyEvent e) {
				logger.trace("method changed: " + getJobImpl());
				paramMap = TrpGuiPrefs.getLaParameters(getJobImpl());
				if(configDialog != null) {
					configDialog.close();
					configDialog = null;
				}
				updateGui();
			}
		});
		if(methodCombo instanceof LabeledComboWithButton) {
			((LabeledComboWithButton)methodCombo).getButton().addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					super.widgetSelected(e);
					if(configDialog != null) {
						configDialog.setVisible();
					} else {
						JobImpl impl = getJobImpl();
						switch(impl) {
						case CITlabAdvancedLaJob:
							configDialog = new CITlabAdvancedLaConfigDialog(getShell(), paramMap, impl);
							break;							
						case TranskribusLaJob:
						case TranskribusLaJobNextGen:		
							configDialog = new TranskribusLaConfigDialog(getShell(), paramMap, impl);
							break;							
						default:
							return;	
						}
						final int ret = configDialog.open();
						logger.debug("Dialog ret = " + ret);
						if(ret == IDialogConstants.OK_ID && configDialog != null) { //may be null if close() is called programmatically (happens when switching methods)
							paramMap = configDialog.getParameters();
							TrpGuiPrefs.storeLaParameters(impl, paramMap);
							updateSelectedModelInfoString();
							updateCurrentConfigInfoString();
						}
						configDialog = null;
					}
				}
			});
		}
		
		if (store != null) {
			store.addListener(new IStorageListener() {
				public void handleLoginOrLogout(LoginOrLogoutEvent arg) {
					updateMethods();
				}
			});
		}
	}
	
	private void updateCurrentConfigInfoString() {
		JobImpl impl = getJobImpl();
		
		if (configDialog != null && (JobImpl.CITlabAdvancedLaJob.equals(impl) || isTranskribusLaJob(impl))) {
			currentConfigString = configDialog.getConfigInfoString();
		}
		else if (JobImpl.CITlabAdvancedLaJob.equals(impl)) {
			currentConfigString = new CITlabAdvancedLaConfigDialog(null, paramMap, impl).constructConfigInfoString();
		}
		else if (isTranskribusLaJob(impl)) {
			currentConfigString = new TranskribusLaConfigDialog(null, paramMap, impl).constructConfigInfoString();
		}
		else {
			currentConfigString = null;
		}
	}

	private boolean isTranskribusLaJob(JobImpl impl) {
		return JobImpl.TranskribusLaJob.equals(impl) || JobImpl.TranskribusLaJobNextGen.equals(impl);
	}
	
	private void setPageSelectionToCurrentPage(){
		if (TEST) {
			dps.getPagesSelector().getPagesText().setText("NA");
		} else {
			dps.getPagesSelector().getPagesText().setText(""+store.getPage().getPageNr());	
		}
	}
	
	public void setPageSelectionToSelectedPages(String pages){
		dps.getPagesSelector().getPagesText().setText(pages);
	}
	
	public Button getDoBlockSegBtn() {
		return doBlockSegBtn;
	}

	public Button getDoLineSegBtn() {
		return doLineSegBtn;
	}
	
	public String getSelectedMethod() {
		if (methodCombo.combo.getSelectionIndex()>=0 && methodCombo.combo.getSelectionIndex()<methodCombo.combo.getItemCount()) {
			return methodCombo.combo.getItems()[methodCombo.combo.getSelectionIndex()];	
		} else {
			return "";
		}
	}
	
	public boolean isTranskribusLaSelected() {
		return METHOD_TRANSKRIBUS.equals(getSelectedMethod()) || METHOD_TRANSKRIBUS_NEXTGEN.equals(getSelectedMethod());
	}
	
	public static JobImpl getJobImplForMethod(String selectedMethod) {
		switch(selectedMethod) {
//		case METHOD_CITLAB_ADVANCED:
//			return JobImpl.CITlabAdvancedLaJob;
		case METHOD_PRINTED_BLOCKS:
			return JobImpl.FinereaderLaJob;
		case METHOD_SEPARATORS:
			return JobImpl.FinereaderSepJob;
		case METHOD_TRANSKRIBUS:
			return JobImpl.TranskribusLaJob;
		case METHOD_KRAKEN:
			return JobImpl.KrakenLaJob;		
		case METHOD_TRANSKRIBUS_NEXTGEN:
			return JobImpl.TranskribusLaJobNextGen;
		default:
			return null;
		}
	}
	
	public void updateSelectionChooserForLA(){
		dps.updateGui();
		this.layout(true);
	}
	
	public List<String> getSelectedStructTypes() {
		return structureTagComp.getSelectedStructTypes();
	}
	
	public boolean isDoLineSeg(){
		return doLineSegBtn.getSelection();
	}
	
	public boolean isDoBlockSeg(){
		return doBlockSegBtn.getSelection();
	}
	
	public boolean isEnrichOldTranscript() {
		return enrichOldTranscriptBtn.getSelection();
	}

	public boolean isDoWordSeg() {
		return doWordSegBtn.getSelection();
	}

	public boolean isCurrentTranscript() {
		return dps.isCurrentTranscript();
	}

	/**
	 * @deprecated use {@link #getDocSelectorData()} to get all the details about user selection
	 * @return true, if user selected multiple documents to process
	 */
	public boolean isDocsSelection() {
		return dps.isDocsSelection();
	}

	/**
	 * @deprecated use {@link #getDocSelectorData()} to get all the details about user page selection
	 * @return a {@link eu.transkribus.core.util.PagesStrUtil}-conform page selection String
	 */
	public String getPages(){
		return dps.getPagesStr();
	}
	
	public ParameterMap getParameters() {
		ParameterMap params = new ParameterMap(paramMap); // copy original so it doesn't get messed up with what gets stored!
		// add struct-type param:
		List<String> structTypes = getSelectedStructTypes();
		
		if (isTranskribusLaSelected() && !isDoBlockSeg()) {
			if (!structTypes.isEmpty()) {
				logger.info("adding struct types parameter: "+StringUtils.join(structTypes, ","));
				params.addParameter(JobConst.PROP_PARS_PREFIX+"struct_types", ""+StringUtils.join(structTypes, ","));
			}
			// other region related params
			if (splitOnRegionBodersBtn.getSelection()) {
				params.addParameter(JobConst.PROP_PARS_PREFIX+"split_lines_on_region_borders", "True");
			}
			if (!overlapFractionText.getText().isEmpty()) {
				params.addParameter(JobConst.PROP_PARS_PREFIX+"line_overlap_fraction", ""+overlapFractionText.getText());	
			}
		}
		
		return params;
	}

	/**
	 * @deprecated use {@link #getDocSelectorData()} to get all the details about user selection
	 * @return a list of DocSelection objects
	 */
	public List<DocSelection> getDocs() {
		return dps.getDocSelections();
	}

	/**
	 * @return a {@link CurrentTranscriptOrDocPagesOrCollectionSelector.DocSelectorData} object, containing all the details about pages/documents to be processed.
	 */
	public CurrentTranscriptOrDocPagesOrCollectionSelector.DocSelectorData getDocSelectorData() {
		return dps.getData();
	}
	
	public JobImpl getJobImpl() {
		String selectedMethod = getSelectedMethod();
		JobImpl jobImpl = getJobImplForMethod(selectedMethod);
		return jobImpl;
	}
	
	public boolean isSelectedMethodConfigurable() {
		return isTranskribusLaJob(getJobImpl()) || JobImpl.CITlabAdvancedLaJob.equals(getJobImpl());
	}
	
	public String getSelectedModelInfoString() {
		if (configDialog != null && (JobImpl.CITlabAdvancedLaJob.equals(getJobImpl()) || isTranskribusLaJob(getJobImpl()))) {
			return configDialog.getSelectedModelInfoString();
		}
		else if (isTranskribusLaJob(getJobImpl())) {
			return new TranskribusLaConfigDialog(getShell(), getParameters(), getJobImpl()).getSelectedModelInfoString();
		}
		else if (JobImpl.CITlabAdvancedLaJob.equals(getJobImpl())) {
			return new CITlabAdvancedLaConfigDialog(getShell(), getParameters(), getJobImpl()).getSelectedModelInfoString();
		}
		else {
			return "";
		}
	}
	
	public String getConfigInfoString() {
		String configInfoStr="";
		
		if (isSelectedMethodConfigurable()) {
			List<String> rids = TrpMainWidget.i() != null ? TrpMainWidget.i().getSelectedRegionIds() : null;
			if (isCurrentTranscript() && !CoreUtils.isEmpty(rids)) {
				configInfoStr+="\nSelected regions: "+StringUtils.join(rids, ",")+"\n";
			}
			List<String> structTypes = getSelectedStructTypes();
			if (isTranskribusLaSelected() && !isDoBlockSeg()) {
				configInfoStr+="\nRestrict on struct types: "+StringUtils.join(structTypes,",")+"\n";	
			}
			if (currentConfigString != null) {
				configInfoStr += currentConfigString;
			}
		}

		return configInfoStr;
	}
}
