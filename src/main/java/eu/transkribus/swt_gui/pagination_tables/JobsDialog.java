package eu.transkribus.swt_gui.pagination_tables;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import eu.transkribus.swt.util.SWTUtil;

public class JobsDialog extends Dialog {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(JobsDialog.class);

	public JobTableWidgetPagination jw;
	JobTableWidgetListener jwl;

	private String selectedText = null;

	public JobsDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, true));

		jw = new JobTableWidgetPagination(container, 0, 50);
		jw.setLayoutData(new GridData(GridData.FILL_BOTH));
		jwl = new JobTableWidgetListener(jw);
		addContextMenu();

		container.pack();

		return container;
	}

	private void addContextMenu() {
		Menu menu = jw.getPageableTable().getViewer().getTable().getMenu();
		MenuItem mi = new MenuItem(menu, SWT.PUSH);
		mi.setText("Copy to clipboard");
		Table table = jw.getPageableTable().getViewer().getTable();

		// save text of cell under mouse on mouse-down:
		table.addListener(SWT.MouseDown, e -> {
			Point pt = new Point(e.x, e.y);
			TableItem item = table.getItem(pt);
			logger.trace("mouse-down, pt = " + pt + " item = " + item);
			if (item != null) {
				for (int c = 0; c < table.getColumnCount(); c++) {
					Rectangle rect = item.getBounds(c);
					if (rect.contains(pt)) {
						selectedText = item.getText(c);
					}
				}
			}
		});

		// copy selectedText to clipboard on menu-item selection:
		SWTUtil.onSelectionEvent(mi, e -> {
			if (selectedText != null) {
				logger.debug("Copying text to clipboard: " + selectedText);
				Clipboard clipboard = new Clipboard(Display.getDefault());
				TextTransfer textTransfer = TextTransfer.getInstance();
				clipboard.setContents(new String[] { selectedText }, new TextTransfer[] { textTransfer });
				clipboard.dispose();
			}
		});
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		SWTUtil.centerShell(shell);
		shell.setText("Jobs on Server");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(1000, 800);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
		setBlockOnOpen(false);
	}

}
