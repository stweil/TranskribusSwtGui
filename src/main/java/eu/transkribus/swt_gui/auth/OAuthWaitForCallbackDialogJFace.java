package eu.transkribus.swt_gui.auth;

import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @deprecated
 * Failed to implement the blocking login call in JFace Dialog.
 */
public class OAuthWaitForCallbackDialogJFace extends Dialog {
	private static final Logger logger = LoggerFactory.getLogger(OAuthWaitForCallbackDialogJFace.class);
	protected Shell shell;
	private Button helpButton;
	private final Thread loginThread;

	/**
	 * Create the dialog.
	 * @param parent
	 */
	public OAuthWaitForCallbackDialogJFace(Shell parent, final Runnable loginRunnable) {
		super(parent);
		loginThread = new Thread(new Runnable() {
			@Override
			public void run() {
				loginRunnable.run();
				logger.debug("Trying to close dialog.");
				//trigger okPressed when the runnable finished, i.e. the login is complete
				Display.getCurrent().asyncExec(() -> okPressed());
			}
		});
	}

	public void setVisible() {
		if (super.getShell() != null && !super.getShell().isDisposed()) {
			super.getShell().setVisible(true);
		}
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Login to Transkribus...");
	}

	@Override
	protected Point getInitialSize() {
		return SWTUtil.getPreferredOrMinSize(getShell(), 450, 200);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.HELP);
	}


	/*
	 * Add help button to buttonBar
	 *
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		helpButton = createButton(parent, IDialogConstants.HELP_ID, "Help", false);
		helpButton.setImage(Images.HELP);

//		createButton(parent, IDialogConstants.OK_ID, "OK", true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel" , true);
		GridData buttonLd = (GridData)getButton(IDialogConstants.CANCEL_ID).getLayoutData();
		helpButton.setLayoutData(buttonLd);
//		helpButton.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				DesktopUtil.browse(HELP_WIKI_PAGE, "You can find the relevant information on the Transkribus Wiki.", getParentShell());
//			}
//		});
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(cont);
		Label info = new Label(cont, SWT.NONE);
		info.setText("Please follow the instructions in your browser to login.");
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).span(1, 1).applyTo(info);
		return cont;
	}

	@Override
	public int open() {
		loginThread.start();
		logger.debug("Login flow started, opening wait dialog...");
		return super.open();
	}

	@Override
	public boolean close() {
		if(getReturnCode() != IDialogConstants.OK_ID) {
			cancel();
		}
		return super.close();
	}

	protected void cancel() {
		if(loginThread.isAlive() && !loginThread.isInterrupted()) {
			loginThread.interrupt();
			logger.debug("Interrupted login thread.");
		}
	}
}
