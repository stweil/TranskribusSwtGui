package eu.transkribus.swt_gui.auth;

import eu.transkribus.client.connection.TrpServer;
import eu.transkribus.client.oidc.ITokenStorage;
import eu.transkribus.core.exceptions.ClientVersionNotSupportedException;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.swt.util.Colors;
import eu.transkribus.swt.util.DesktopUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.databinding.DataBinder;
import eu.transkribus.swt_gui.TrpGuiPrefs;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.mainwidget.settings.TrpSettings;
import eu.transkribus.swt_gui.transcription.autocomplete.TrpAutoCompleteField;
import eu.transkribus.swt_gui.util.DelayedTask;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.List;

public class LoginDialog extends Dialog {
	private final static Logger logger = LoggerFactory.getLogger(LoginDialog.class);
	public final static String TRANSKRIBUS_X_CLIENT_ID = "transkribus-swt-gui";
	public static final String TRANSKRIBUS_ACCOUNT_TYPE = "Transkribus";
	public static final String TRANSKRIBUS_SSO_ACCOUNT_TYPE = "Transkribus Single Sign-On";
	private static final String CONNECT_NEW_ACCOUNT = "Connect account...";

	/**
	 * @deprecated as the auth server base url is now attached to {@link TrpServer}
	 */
	private final static String RESET_PW_URL_BASE = "https://account.readcoop.eu/auth/realms/readcoop/login-actions/reset-credentials?client_id=account";

	protected Composite container;
	protected Combo accountCombo;
	protected Group grpCreds;
	protected Text txtUser;
	protected Text txtPassword;
	protected CCombo serverCombo;
	protected Button rememberCredentials;
	protected Button clearStoredCredentials;
	protected Button autoLogin;

	protected final String message;
	protected String[] userProposals;

	protected Label infoLabel;
	protected String[] serverProposals;
	protected int defaultUriIndex;

	private Combo ssoUsernameCombo;
	private Button ssoDisconnectBtn;

	private Button useSystemBrowserChk;

	private String selectedServer;

	TrpAutoCompleteField autocomplete;

	private final TrpMainWidget mw;

	private final static boolean SHOW_ACCOUNT_TYPE_COMBO = true;

	public LoginDialog(Shell parentShell, TrpMainWidget mainWidget, String message, String[] userProposals, String[] serverProposals,
			int defaultUriIndex) {
		super(parentShell);
		this.message = message;
		this.userProposals = userProposals;
		if(serverProposals == null) {
			serverProposals = new String[]{};
		}
		this.serverProposals = serverProposals;
		this.defaultUriIndex = CoreUtils.bound(defaultUriIndex, 0, serverProposals.length - 1);
		this.mw = mainWidget;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);

		if(SHOW_ACCOUNT_TYPE_COMBO) {
			Label lblAccount = new Label(container, SWT.NONE);
			lblAccount.setText("Account type:");
			accountCombo = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);
			accountCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			initAccountCombo();
		}

		grpCreds = new Group(container, SWT.NONE);
		clearGrpCreds(2);

		if(doShowServerCombo()) {
			initServerCombo(container);
		} else {
			selectedServer = serverProposals[0];
		}

		autoLogin = new Button(container, SWT.CHECK);
		autoLogin.setText("Auto login");
		autoLogin.setToolTipText("Auto login with last remembered credentials on next startup");

		new Label(container, 0);

		infoLabel = new Label(container, SWT.FLAT);
		infoLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		infoLabel.setForeground(Colors.getSystemColor(SWT.COLOR_DARK_RED));
		infoLabel.setText(message);

		if(SHOW_ACCOUNT_TYPE_COMBO) {
			final String accType = TrpGuiPrefs.getLastLoginAccountType();
			setAccountComboSelection(accType);
		} else {
			setAccountType(TRANSKRIBUS_ACCOUNT_TYPE);
		}

		postInit();

		return container;
	}

	private boolean doShowServerCombo() {
		return serverProposals.length < 1 || serverProposals.length > 1;
	}

	private void initAccountCombo() {
		accountCombo.add(TRANSKRIBUS_ACCOUNT_TYPE);
		accountCombo.add(TRANSKRIBUS_SSO_ACCOUNT_TYPE);
		accountCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setAccountType(accountCombo.getText());
			}
		});
	}

	private void setAccountComboSelection(String accType) {
		int selectionIndex = 0;
		for(int i = 0; i < accountCombo.getItemCount(); i++) {
			if(accountCombo.getItem(i).equals(accType)) {
				selectionIndex = i;
				break;
			}
		}
		accountCombo.select(selectionIndex);
		setAccountType(accountCombo.getText());
	}

	public String getAccountComboSelection() {
		if(!SHOW_ACCOUNT_TYPE_COMBO) {
			return TRANSKRIBUS_ACCOUNT_TYPE;
		}
		return accountCombo.getText();
	}
	
	private void setAccountType(final String accountType){
		logger.debug("Setting account type = {}", accountType);
		if(accountType == null || TRANSKRIBUS_ACCOUNT_TYPE.equals(accountType)) {
			initTranskribusAccountFields();
			updateLoginButtonEnabledState(true);
		} else if(TRANSKRIBUS_SSO_ACCOUNT_TYPE.equals(accountType)) {
			initSsoAccountFields();
			updateLoginButtonEnabledState(true);
			updateSsoDisconnectBtnEnabledState();
		}
		container.layout();
	}

	private void updateLoginButtonEnabledState(boolean enabled) {
		//enable OK button only if Transkribus account is selected
		if(getButton(IDialogConstants.OK_ID) != null) {
			getButton(IDialogConstants.OK_ID).setEnabled(enabled);
		}
	}

	private void initTranskribusAccountFields() {

		clearGrpCreds(2);

		Label lblUser = new Label(grpCreds, SWT.NONE);
		lblUser.setText("User:");

		txtUser = new Text(grpCreds, SWT.BORDER);
		txtUser.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtUser.setText("");

		Label lblPassword = new Label(grpCreds, SWT.NONE);
		GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblNewLabel.horizontalIndent = 1;
		lblPassword.setLayoutData(gd_lblNewLabel);
		lblPassword.setText("Password:");

		txtPassword = new Text(grpCreds, SWT.BORDER | SWT.PASSWORD);
		txtPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtPassword.setText("");

		autocomplete = new TrpAutoCompleteField(txtUser, new TextContentAdapter(), new String[] {},
				KeyStroke.getInstance(SWT.CTRL, SWT.SPACE), null);
		autocomplete.getAdapter().setEnabled(true);
		autocomplete.getAdapter().setPropagateKeys(true);


		rememberCredentials = new Button(grpCreds, SWT.CHECK);
		rememberCredentials.setText("Remember credentials");
		rememberCredentials.setToolTipText("If login is successful those credentials will be stored");

		Composite btnGrp = new Composite(grpCreds, SWT.NONE);
		GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(2).equalWidth(false).applyTo(btnGrp);
		GridDataFactory.create(GridData.FILL_BOTH).applyTo(btnGrp);

		Button resetPwBtn = new Button(btnGrp, SWT.PUSH);
		resetPwBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		resetPwBtn.setText("Forgot password?");
		resetPwBtn.setToolTipText("Opens the password reset page in a new browser tab");
//		resetPwBtn.setImage(Images.getOrLoad("/icons/world.png"));

		clearStoredCredentials = new Button(btnGrp, SWT.PUSH);
		clearStoredCredentials.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		clearStoredCredentials.setText("Clear stored credentials");
		clearStoredCredentials.setToolTipText("Clears previously stored credentials from harddisk");

		txtUser.addModifyListener(new ModifyListener() {
			@Override public void modifyText(ModifyEvent e) {
				updateCredentialsOnTypedUser();
			}
		});

		txtPassword.addFocusListener(new FocusListener() {
			@Override public void focusLost(FocusEvent e) {
			}

			@Override public void focusGained(FocusEvent e) {
				updateCredentialsOnTypedUser();
			}
		});

		resetPwBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				final String resetPwUrl;
				if(!StringUtils.isEmpty(txtUser.getText())) {
					resetPwUrl = RESET_PW_URL_BASE + "&prefillUsername=" + txtUser.getText();
				} else {
					resetPwUrl = RESET_PW_URL_BASE;
				}
				DesktopUtil.browse(resetPwUrl,
						"Sorry, we could not identify your browser.\nTo reset your password, please go to:\n\n" + RESET_PW_URL_BASE,
						LoginDialog.this.getParentShell());
			}
		});

		clearStoredCredentials.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				try {
					TrpGuiPrefs.clearCredentials();
				} catch (Exception e1) {
					logger.error(e1.getMessage());
				}
			}
		});

		// update credentials for default user if any:
		Pair<String, String> storedCreds = TrpGuiPrefs.getStoredCredentials(null);
		if (storedCreds != null) {
			logger.debug("found stored creds for user: " + storedCreds.getLeft());
			setUsername(storedCreds.getLeft());
			setPassword(storedCreds.getRight());
		}

		setInfo(message);
	}


	private void initSsoAccountFields(){
		clearGrpCreds(3);
		Label userLabel = new Label(grpCreds, SWT.FLAT);
		GridDataFactory.fillDefaults()
				.grab(false, false)
				.span(1, 1)
				.align(SWT.LEFT, SWT.CENTER)
				.applyTo(userLabel);

		userLabel.setText("User:");
		ssoUsernameCombo = buildSsoUsernameCombo();
		updateSsoUsernameComboValues();
		ssoDisconnectBtn = buildSsoDisconnectBtn();

		//swt browser does not yet work with this. Always use system browser.
//		useSystemBrowserChk = new Button(grpCreds, SWT.CHECK);
//		useSystemBrowserChk.setText("Use system browser");
//		useSystemBrowserChk.setSelection(true);

		for(int i = 0; i < 9; i++) {
			//just fill this up with labels for now
			Label emptyLbl = new Label(grpCreds, SWT.NONE);
			GridDataFactory.create(GridData.FILL_VERTICAL).applyTo(emptyLbl);
		}
		setInfo("Select an account or connect to a new one");
	}

	private Combo buildSsoUsernameCombo() {
		Combo ssoUsernameCombo = new Combo(grpCreds, SWT.READ_ONLY);
		GridDataFactory.fillDefaults()
				.grab(true, false)
				.span(1, 1)
				.align(SWT.FILL, SWT.CENTER)
				.applyTo(ssoUsernameCombo);
		ssoUsernameCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateLoginButtonEnabledState(true);
				updateSsoDisconnectBtnEnabledState();
			}
		});
		return ssoUsernameCombo;
	}

	private void updateSsoUsernameComboValues() {
		ITokenStorage tokenStore = getSsoTokenStore();
		List<String> usernames = tokenStore.getUsernames(getAuthServerUrl());
		ssoUsernameCombo.removeAll();
		//data of the items is used as "login hint" (prefill value) for the account page's username input
		for(String username : usernames) {
			ssoUsernameCombo.add(username);
			ssoUsernameCombo.setData(username, username);
		}
		ssoUsernameCombo.add(CONNECT_NEW_ACCOUNT);
		ssoUsernameCombo.setData(CONNECT_NEW_ACCOUNT, null);
		final String lastUsername = TrpGuiPrefs.getLastLogin();
		int selectionIndex = 0;
		for(int i = 0; i < ssoUsernameCombo.getItemCount(); i++) {
			if (ssoUsernameCombo.getItem(i).equals(lastUsername)) {
				selectionIndex = i;
				break;
			}
		}
		ssoUsernameCombo.select(selectionIndex);
	}

	protected ITokenStorage getSsoTokenStore() {
		return mw.getStorage().getSsoTokenStore();
	}

	private Button buildSsoDisconnectBtn() {
		Button ssoDisconnectBtn = new Button(grpCreds, SWT.PUSH);
		ssoDisconnectBtn.setImage(Images.DISCONNECT);
		ssoDisconnectBtn.setToolTipText("Logout and disconnect the selected account");
		GridDataFactory.fillDefaults()
				.grab(false, false)
				.span(1, 1)
				.align(SWT.RIGHT, SWT.CENTER)
				.applyTo(ssoDisconnectBtn);
		ssoDisconnectBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final String ssoUsername = getSelectedSsoUsername();
				final String authServerUrl = getAuthServerUrl();
				try {
					mw.disconnectSsoAccount(authServerUrl, ssoUsername);
					updateSsoUsernameComboValues();
					updateSsoDisconnectBtnEnabledState();
				} catch (IOException ex) {
					setInfo("Failed to disconnect account. Please try again later or report a bug.");
				}
			}
		});
		return ssoDisconnectBtn;
	}

	private void updateSsoDisconnectBtnEnabledState() {
		String ssoUsername = getSelectedSsoUsername();
		ssoDisconnectBtn.setEnabled(ssoUsername != null);
	}

	private String getSelectedSsoUsername() {
		return (String) ssoUsernameCombo.getData(ssoUsernameCombo.getText());
	}

	private String getAuthServerUrl() {
		final String serverStr = getSelectedServer();
		final String authServerUriStr = TrpServer.resolveAuthServerUriStr(serverStr);
		logger.debug("Resolved auth server: {} -> {}", serverStr, authServerUriStr);
		return authServerUriStr;
	}

	private void clearGrpCreds(int numColsToInit) {
		for(Control c : grpCreds.getChildren()){
			c.dispose();
		}
		grpCreds.setLayout(new GridLayout(numColsToInit, false));
		grpCreds.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 3));
	}

	private void initServerCombo(Composite container) {
		Label serverLabel = new Label(container, SWT.FLAT);
		serverLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		serverLabel.setText("Server: ");

		serverCombo = new CCombo(container, SWT.DROP_DOWN);
		serverCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		for (String s : serverProposals) {
			serverCombo.add(s);
		}
		if (serverProposals.length > 0) {
			serverCombo.select(0);
		}
		if (defaultUriIndex >= 0 && defaultUriIndex < serverProposals.length) {
			serverCombo.select(defaultUriIndex);
		}
		selectedServer = serverCombo.getText();

		final DelayedTask dt = new DelayedTask(() -> {
			final String prev = selectedServer;
			selectedServer = serverCombo.getText();
			logger.trace("Server combo text changed: {} -> {}", prev, selectedServer);
			if(TRANSKRIBUS_SSO_ACCOUNT_TYPE.equals(getAccountComboSelection())) {
				updateSsoUsernameComboValues();
			}
		}, true);
		serverCombo.addModifyListener(e -> dt.start());
	}

	public void setInfo(String info) {
		infoLabel.setText(info);
	}

	public String getSelectedServer() {
		return selectedServer;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Login");
	}

	// override method to use "Login" as label for the OK button
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Login", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		int initialHeight = 300;
		if(doShowServerCombo()) {
			initialHeight = 330;
		}
		return new Point(500, initialHeight);
	}

	public String getUser() {
		return txtUser.getText();
	}

	public void setPassword(String pw) {
		txtPassword.setText(pw);
	}

	public void setUsername(String username) {
		txtUser.setText(username);
	}

	public char[] getPassword() {
		return txtPassword.getText().toCharArray();
	}

	public boolean isRememberCredentials() {
		return rememberCredentials.getSelection();
	}

	public boolean isDisposed() {
		return getShell() == null || getShell().isDisposed();
	}

	void updateCredentialsOnTypedUser() {
		Pair<String, String> storedCreds = TrpGuiPrefs.getStoredCredentials(getUser());
		if (storedCreds != null) {
			setPassword(storedCreds.getRight());
		} else {
			setPassword("");
		}
	}

	@Override protected void okPressed() {
		String server = getSelectedServer();
		boolean success = false;
		String errorMsg = "";
		try {
			if(SHOW_ACCOUNT_TYPE_COMBO && TRANSKRIBUS_SSO_ACCOUNT_TYPE.equals(getAccountComboSelection())) {
				success = loginSso(server);
			} else {
				success = loginClassic(server);
			}
		}
		catch (ClientVersionNotSupportedException e) {
			logger.error(e.getMessage(), e);
			close();
			String errorMsgStripped = StringUtils.removeStart(e.getMessage(), "Client error: ");
			mw.notifyOnRequiredUpdate(errorMsgStripped);
			return;
		}
		catch (LoginException e) {
			mw.logout(true, false);
			logger.error(e.getMessage(), e);
			success = false;
			errorMsg = e.getMessage();
		}
		catch (IllegalStateException e) {
			mw.logout(true, false);
			logger.error(e.getMessage(), e);
			success = false;
			errorMsg = e.getMessage();
			if("Already connected".equals(e.getMessage()) && e.getCause() != null) {
				/*
				 * Jersey throws an IllegalStateException "Already connected" for a variety of issues where actually no connection can be established.
				 * see https://github.com/jersey/jersey/issues/3000
				 */
				Throwable cause = e.getCause();
				//override misleading "Already connected" message
				errorMsg = cause.getMessage();
				logger.error("'Already connected' caused by: " + cause.getMessage(), cause);
			}
		}
		catch (Exception e) {
			mw.logout(true, false);
			logger.error(e.getMessage(), e);
			success = false;
			errorMsg = e.getMessage();
		}

		if (success) {
			close();
			mw.onSuccessfullLoginAndDialogIsClosed();
		} else {
			String msg = StringUtils.isEmpty(errorMsg) ? "Login failed" : "Login failed: "+errorMsg;
			setInfo(msg);
		}
	}

	private boolean loginSso(final String server) {
		String usernameHint = null;
		if(ssoUsernameCombo.getData(ssoUsernameCombo.getText()) instanceof String) {
			usernameHint = (String) ssoUsernameCombo.getData(ssoUsernameCombo.getText());
		}
		return mw.loginSso(server, usernameHint, useSystemBrowserChk == null || useSystemBrowserChk.getSelection());
	}

	boolean loginClassic(String server) throws LoginException {
		String user = getUser();
		char[] pw = getPassword();
		boolean rememberCreds = isRememberCredentials();
		boolean success = mw.login(server, user, String.valueOf(pw), rememberCreds);

		//with transition to the new website, the google login is deactivated (see LoginDialog).
		//on default login make sure that any access token from before is revoked and cleared.
		TrpGuiPrefs.OAuthCreds creds = TrpGuiPrefs.getOAuthCreds();
		if(success && creds != null) {
			TrpGuiPrefs.clearOAuthToken();
			try {
				OAuthGuiUtil.revokeOAuthToken(creds.getRefreshToken());
			} catch (Exception e) {
				//do never fail here
				logger.warn("Revoking Google refresh token failed: {}", e.getMessage());
			}
		}
		return success;
	}

	protected void postInit() {
		try {
			DataBinder db = DataBinder.get();
			db.bindBeanToWidgetSelection(TrpSettings.AUTO_LOGIN_PROPERTY, mw.getTrpSets(), autoLogin);
		} catch(AssertionFailedException e) {
			logger.error("Could not get DataBinder for autologin setting.", e);
		}
	}
}