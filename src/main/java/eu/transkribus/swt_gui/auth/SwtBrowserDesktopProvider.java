package eu.transkribus.swt_gui.auth;

import eu.transkribus.client.oidc.TrpSsoHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class SwtBrowserDesktopProvider implements TrpSsoHandler.IDesktopProvider {
    private static final Logger logger = LoggerFactory.getLogger(SwtBrowserDesktopProvider.class);

    public void browse(URI uri) throws IOException {
        final Display display;
        final boolean doDisposeDisplay;
        if (Display.getCurrent() != null) {
            display = Display.getCurrent();
            doDisposeDisplay = false;
        } else {
            logger.debug("Creating new display.");
            display = new Display();
            doDisposeDisplay = true;
        }

        Shell shell = new Shell(display);
        shell.setSize(800, 930);

        Browser browser = new Browser(shell, SWT.NONE);
        browser.setLayoutData(new GridData(GridData.FILL_BOTH));
        browser.setBounds(0, 0, shell.getSize().x, shell.getSize().y);
        logger.info("Instantiated browser of type = {}", browser.getBrowserType());
        browser.addOpenWindowListener(event -> {
            Shell shell1 = new Shell(display);
            shell1.setText("New Window");
            shell1.setLayout(new GridLayout(1, true));
            Browser browser1 = new Browser(shell1, SWT.NONE);
            browser1.setLayoutData(new GridData(GridData.FILL_BOTH));
            event.browser = browser1;
        });

        browser.setUrl(uri.toString());

        browser.layout();
        shell.open();

        //TODO how to interrupt login (running http listener) when user closes the window?

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        if (doDisposeDisplay) {
            display.dispose();
        }
    }
}
