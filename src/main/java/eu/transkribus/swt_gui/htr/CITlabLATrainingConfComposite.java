package eu.transkribus.swt_gui.htr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;

import eu.transkribus.core.model.beans.CITlabLaTrainConfig;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.Fonts;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.models.ModelChooserButton;

public class CITlabLATrainingConfComposite extends Composite {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(CITlabLATrainingConfComposite.class);
	
	private Text numEpochsTxt;
	private Text learningReateTxt;
	private Combo modelTypeCombo;
	private ModelChooserButton baseModelBtn;
	
	private ExpandableComposite advancedParamsExp;
	private Text scaleMin, scaleMax, scaleVal;
	private LabeledText affValText, elValXText, elValYText;
	private Button affineTr, elasticTr;
	private Button rotateNoneBtn, rotate90Btn, rotateBtn, skeletBtn;
	private LabeledText dilateNumText;

	// model parameters:
	private LabeledText scaleSpaceNumText, resDepthText, featRootText, filterSizeText, numScalesText;
	
	public static final String[] MODEL_TYPE_ITEMS =  {"u", "ru", "aru", "laru"};

	public CITlabLATrainingConfComposite(Composite parent, int style) {
		super(parent, style);
		// setLayout(new GridLayout(1, false));
		setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		ScrolledComposite sc = new ScrolledComposite(this, SWT.V_SCROLL | SWT.H_SCROLL);
		sc.setLayoutData(new GridData(GridData.FILL_BOTH));
		sc.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		Composite content = new Composite(sc, SWT.NONE);
		content.setLayout(new GridLayout(2, false));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));		
		
		Label numEpochsLbl = new Label(content, SWT.NONE);
		numEpochsLbl.setText("Nr. of Epochs:");
		numEpochsTxt = new Text(content, SWT.BORDER);
		numEpochsTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		numEpochsTxt.setToolTipText("The number of epochs");
		
		Label learningRateLbl = new Label(content, SWT.NONE);
		learningRateLbl.setText("Learning rate:");		
		learningReateTxt = new Text(content, SWT.BORDER);
		learningReateTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		learningReateTxt.setToolTipText("The learning rate");
		
		Label baseModelLbl = new Label(content, SWT.NONE);
		baseModelLbl.setText("Base Model:");
		baseModelBtn = new ModelChooserButton(content, true, ModelUtil.TYPE_LAYOUT, getProvider());
		baseModelBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));		
			
		if (Storage.i().isAdminLoggedIn()) {
			initAdvancedParamsComposite(content);
		}
		
//		Button paperLinkBtn = new Button(this, 0);
//		paperLinkBtn.setText("Link to paper...");
//		paperLinkBtn.setImage(Images.SCRIPT_ICON);
//		SWTUtil.onSelectionEvent(paperLinkBtn, e -> {
//			org.eclipse.swt.program.Program.launch("https://arxiv.org/pdf/1802.03345v2.pdf");
//		});
		
//		Button codeLinkBtn = new Button(this, 0);
//		codeLinkBtn.setText("Link to GitHub repo...");
//		codeLinkBtn.setImage(Images.SCRIPT_CODE_ICON);
//		SWTUtil.onSelectionEvent(codeLinkBtn, e -> {
//			org.eclipse.swt.program.Program.launch("https://github.com/TobiasGruening/ARU-Net");
//		});		
		
		setDefault();

		sc.setContent(content);
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
		content.layout();
	    sc.setMinSize(0, content.computeSize(SWT.DEFAULT, SWT.DEFAULT).y-1);		
	}
	
	private void initAdvancedParamsComposite(Composite container) {
		advancedParamsExp = new ExpandableComposite(container, ExpandableComposite.COMPACT);
//		advancedParamsExp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		advancedParamsExp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));	
		Composite grp = new Composite(advancedParamsExp, SWT.SHADOW_ETCHED_IN);
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grp.setLayout(new GridLayout(2, false));
		
//		LabeledText batchSize = new LabeledText(grp, "Batch Size: ");
//		batchSize.setText("1");

		Group grpDataAug = new Group(grp, SWT.NONE);
		grpDataAug.setText("Data Augmentation / Preprocessing");
		grpDataAug.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		grpDataAug.setLayout(new GridLayout(1, false));
		Fonts.setBoldFont(grpDataAug);

		Composite scaleComp = new Composite(grpDataAug, SWT.NONE);
		scaleComp.setLayout(SWTUtil.createGridLayout(4, false, 0, 0));
		scaleComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label scaleLbl = new Label(scaleComp, SWT.NONE);
		scaleLbl.setText("Scaling min/max/validation: ");
		scaleLbl.setToolTipText("Scaling factors used for data augmentation - images are scaled randomly between min and max, validation images are scaled by the validation factor");

		scaleMin = new Text(scaleComp, SWT.BORDER | SWT.SINGLE);
		scaleMin.setText("0.2");
		scaleMin.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		scaleMin.setToolTipText("Minimal scaling parameter during data augmentation");
		
		scaleMax = new Text(scaleComp, SWT.BORDER | SWT.SINGLE);
		scaleMax.setText("0.5");
		scaleMax.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		scaleMax.setToolTipText("Maximal scaling parameter during data augmentation");
		
		scaleVal = new Text(scaleComp, SWT.BORDER | SWT.SINGLE);
		scaleVal.setText("0.33");
		scaleVal.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		scaleVal.setToolTipText("Scaling parameter used for validation");
		
		Composite affComp = new Composite(grpDataAug, SWT.NONE);
		affComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		affComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		affineTr = new Button(affComp, SWT.CHECK);
		affineTr.setText("Affine transformations");
		affineTr.setSelection(true);
		// affineTr.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		affineTr.setToolTipText("Perform affine transformations for data augmentation?");

		affValText = new LabeledText(affComp, "Value: ");
		affValText.setText("0.025");
		affValText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		affValText.setToolTipText("Affine transformation strength");
		
		affValText.setEnabled(affineTr.getSelection());
		SWTUtil.onSelectionEvent(affineTr, e -> {
			affValText.setEnabled(affineTr.getSelection());
		});		

		Composite elComp = new Composite(grpDataAug, SWT.NONE);
		elComp.setLayout(SWTUtil.createGridLayout(3, false, 0, 0));
		elComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		elasticTr = new Button(elComp, SWT.CHECK);
		elasticTr.setText("Elastic transformations");
		elasticTr.setSelection(false);
		// elasticTr.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		elasticTr.setToolTipText("Perform elastic transformations for data augmentation?");
		
		elValXText = new LabeledText(elComp, "Value X: ");
		elValXText.setText("0.0002");
		elValXText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		elValXText.setToolTipText("Elastic transformation strength in X-direction");

		elValYText = new LabeledText(elComp, "Value Y: ");
		elValYText.setText("0.0002");
		elValYText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		elValYText.setToolTipText("Elastic transformation strength in Y-direction");

		elValXText.setEnabled(elasticTr.getSelection());
		elValYText.setEnabled(elasticTr.getSelection());
		SWTUtil.onSelectionEvent(elasticTr, e -> {
			elValXText.setEnabled(elasticTr.getSelection());
			elValYText.setEnabled(elasticTr.getSelection());
		});

		Composite rotateComp = new Composite(grpDataAug, SWT.NONE);
		rotateComp.setLayout(SWTUtil.createGridLayout(4, false, 0, 0));
		rotateComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		rotateComp.setToolTipText("Perform rotations for data augmentation?");

		Label rotateLbl = new Label(rotateComp, SWT.NONE);
		rotateLbl.setText("Rotations: ");

		rotateNoneBtn = new Button(rotateComp, SWT.RADIO);
		rotateNoneBtn.setText("None");
		rotateNoneBtn.setSelection(true);
		rotateNoneBtn.setToolTipText("Do not perform rotations for data augmentation?");

		rotate90Btn = new Button(rotateComp, SWT.RADIO);
		rotate90Btn.setText("90°");
		rotate90Btn.setToolTipText("Rotate images by random multiples of 90° for data augmentation?");

		rotateBtn = new Button(rotateComp, SWT.RADIO);
		rotateBtn.setText("Random");
		rotateBtn.setToolTipText("Rotate images by random angles for data augmentation?");

		Composite skeletComp = new Composite(grpDataAug, SWT.NONE);
		skeletComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		skeletComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		skeletBtn = new Button(skeletComp, SWT.CHECK);
		skeletBtn.setText("Skeletonization");
		skeletBtn.setSelection(true);
		skeletBtn.setToolTipText("Perform skeletonization on the images?");
		// skeletBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		dilateNumText = new LabeledText(skeletComp, "Dilations: ");
		dilateNumText.setText("1");
		dilateNumText.setToolTipText("Number of dilations to perform on the skeletonized image");
		dilateNumText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		dilateNumText.setEnabled(skeletBtn.getSelection());
		SWTUtil.onSelectionEvent(skeletBtn, e -> {
			dilateNumText.setEnabled(skeletBtn.getSelection());
		});		

		Group grpModel = new Group(grp, SWT.NONE);
		grpModel.setText("Model");
		grpModel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		grpModel.setLayout(new GridLayout(1, false));
		Fonts.setBoldFont(grpModel);

		Composite modelTypeComp = new Composite(grpModel, SWT.NONE);
		modelTypeComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		modelTypeComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label modelTypeLbl = new Label(modelTypeComp, SWT.NONE);
		modelTypeLbl.setText("Model type:");			
		modelTypeCombo = new Combo(modelTypeComp, SWT.READ_ONLY);
		modelTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		modelTypeCombo.setToolTipText("The model type - cf. paper: https://arxiv.org/pdf/1802.03345v2.pdf");
		modelTypeCombo.setItems(MODEL_TYPE_ITEMS);		

		scaleSpaceNumText = new LabeledText(grpModel, "Scale spaces: ");
		scaleSpaceNumText.setText("6");
		scaleSpaceNumText.setToolTipText("The number of scale spaces");
		scaleSpaceNumText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		resDepthText = new LabeledText(grpModel, "Residual depth: ");
		resDepthText.setText("3");
		resDepthText.setToolTipText("The depth of the residual blocks");
		resDepthText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		featRootText = new LabeledText(grpModel, "Feature root: ");
		featRootText.setText("8");
		featRootText.setToolTipText("Number of features in the first layer");
		featRootText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		filterSizeText = new LabeledText(grpModel, "Filter size: ");
		filterSizeText.setText("3");
		filterSizeText.setToolTipText("The size of the convolution filters");
		filterSizeText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		numScalesText = new LabeledText(grpModel, "Number of scales: ");
		numScalesText.setText("5");
		numScalesText.setToolTipText("Number of scales for the scale space pyramid for the attention model");
		numScalesText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		advancedParamsExp.setClient(grp);
		advancedParamsExp.setText("Advanced Parameters");
		advancedParamsExp.setExpanded(true);
		
		Fonts.setBoldFont(advancedParamsExp);
		advancedParamsExp.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				container.layout();
			}
		});
		
	}
	
	public void setDefault() {
		CITlabLaTrainConfig c = new CITlabLaTrainConfig();
		SWTUtil.set(numEpochsTxt, ""+c.getNumEpochs());
		SWTUtil.set(learningReateTxt, ""+c.getLearningRate());
		if (modelTypeCombo!=null) {
			modelTypeCombo.select(Arrays.asList(MODEL_TYPE_ITEMS).indexOf(c.getModelType()));
		}
		if(baseModelBtn != null) {
			baseModelBtn.setModel(null);
		}		
	}
	
	public String getProvider() {
		return ModelUtil.PROVIDER_CITLAB_BASELINES;
	}

	public List<String> validateParameters(ArrayList<String> errorList) {
		if(errorList == null) {
			errorList = new ArrayList<>();
		}
		if (!StringUtils.isNumeric(numEpochsTxt.getText())) {
			errorList.add("Number of Epochs must be a number!");
		}
		
		return errorList;
	}

	public CITlabLaTrainConfig addParameters(CITlabLaTrainConfig conf) {
		conf.getModelMetadata().setProvider(getProvider());
		conf.setNumEpochs(Integer.parseInt(numEpochsTxt.getText()));
		conf.getModelMetadata().setBaseModelId(baseModelBtn.getModelId());
		if (modelTypeCombo!=null) {
			conf.setModelType(modelTypeCombo.getText());
		}
		if (advancedParamsExp!=null && advancedParamsExp.isExpanded()) {
			logger.debug("adding advanced parameters...");
			ParameterMap advancedParams = new ParameterMap();
			// data augmentation / preprocessing pars:
			advancedParams.addParameter("scale_min", scaleMin.getText());
			advancedParams.addParameter("scale_max", scaleMax.getText());
			advancedParams.addParameter("scale_val", scaleVal.getText());
			advancedParams.addBoolParam("affine_tr", affineTr.getSelection());
			advancedParams.addParameter("affine_value", affValText.getText());
			advancedParams.addBoolParam("elastic_tr", elasticTr.getSelection());
			advancedParams.addParameter("elastic_val_x", elValXText.getText());
			advancedParams.addParameter("elastic_value_y", elValYText.getText());
			advancedParams.addBoolParam("rotate_tr", rotateBtn.getSelection());
			advancedParams.addBoolParam("rotateMod90_tr", rotate90Btn.getSelection());
			advancedParams.addBoolParam("skelet", skeletBtn.getSelection());
			advancedParams.addParameter("dilate_num", dilateNumText.getText());
			// model pars:
			advancedParams.addParameter("scale_space_num", scaleSpaceNumText.getText());
			advancedParams.addParameter("res_depth", resDepthText.getText());
			advancedParams.addParameter("featRoot", featRootText.getText());
			advancedParams.addParameter("filter_size", filterSizeText.getText());
			advancedParams.addParameter("num_scales", numScalesText.getText());
			conf.setAdvancedParams(advancedParams);
		}
		
		return conf;
	}

}
