
package eu.transkribus.swt_gui.htr;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledCombo;
import eu.transkribus.swt_gui.mainwidget.storage.IStorageListener;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;

public class TextRecognitionComposite extends Composite {
	private static final Logger logger = LoggerFactory.getLogger(TextRecognitionComposite.class);
	
	private LabeledCombo methodCombo;
	
	public static final String METHOD_OCR = "OCR";
	public static final String METHOD_TRANSKRIBUS_OCR = "Transkribus OCR (Block-segmentation + Transkribus-Print-M1 model)";
	public static final String METHOD_HTR = "HTR (All engines)";
	public static final String METHOD_TRHTR = "TrHtR";
	
	public static final Map<String, JobImpl> METHOD_TO_JOBIMPL_MAP = new LinkedHashMap<>();
	static {
		METHOD_TO_JOBIMPL_MAP.put(METHOD_HTR, null); // JobImpl == null -> allow method for all users
		METHOD_TO_JOBIMPL_MAP.put(METHOD_TRANSKRIBUS_OCR, null);
//		METHOD_TO_JOBIMPL_MAP.put(METHOD_OCR, JobImpl.FinereaderOcrJob);
	}
	
	Button runBtn;
	
	public TextRecognitionComposite(Composite parent, int style) {
		super(parent, style);
		
		int nCols = 4;
		GridLayout gl = new GridLayout(nCols, false);
		gl.marginHeight = gl.marginWidth = 0;
		this.setLayout(gl);

		methodCombo = new LabeledCombo(this, "Method:");
		methodCombo.combo.setItems(getMethodsForUser());
		methodCombo.combo.select(0);
		methodCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 3));
		
		runBtn = new Button(this, 0);
		runBtn.setText("Run...");
		runBtn.setImage(Images.ARROW_RIGHT);
		runBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		runBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		Storage.getInstance().addListener(new IStorageListener() {
			public void handleLoginOrLogout(LoginOrLogoutEvent arg) {
				methodCombo.combo.setItems(getMethodsForUser());
				methodCombo.combo.select(0);
			}
		});
	}
	
	private String[] getMethodsForUser() {
		List<String> methodsList = new ArrayList<>();
		boolean isAdmin = Storage.i().isAdminLoggedIn();
		for (Entry<String, JobImpl> e : METHOD_TO_JOBIMPL_MAP.entrySet()) {
			if (isAdmin || e.getValue()==null || Storage.i().isUserAllowedForJob(e.getValue().toString(), false)) {
				methodsList.add(e.getKey());
			}
		}
		return methodsList.toArray(new String[methodsList.size()]);
	}
	
	public Button getRunBtn() {
		return runBtn;
	}
	
	public String getSelectedMethod() {
		return methodCombo.txt();
	}
	
	public boolean isTrHtr() {
		return getSelectedMethod().equals(METHOD_TRHTR);
	}
	
	public boolean isOcr() {
		return getSelectedMethod().equals(METHOD_OCR);
	}
	
	public boolean isHtr() {
		return getSelectedMethod().equals(METHOD_HTR);
	}
	
	public boolean isTranskribusOcr() {
		return  getSelectedMethod().equals(METHOD_TRANSKRIBUS_OCR);
	}

}
