package eu.transkribus.swt_gui.htr;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dea.fimgstoreclient.beans.ImgType;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.PyLaiaCreateModelPars;
import eu.transkribus.core.model.beans.PyLaiaHtrTrainConfig;
import eu.transkribus.core.model.beans.PyLaiaTrainCtcPars;
import eu.transkribus.core.model.beans.TextFeatsCfg;
import eu.transkribus.core.model.beans.TrpHtr;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.TrpPreprocPars;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.models.ModelChooserButton;

public class PyLaiaTrainingConfComposite extends Composite {
	private static final Logger logger = LoggerFactory.getLogger(PyLaiaTrainingConfComposite.class);
	
	private Text numEpochsTxt, earlyStoppingTxt, learningRateTxt;
	private Combo imgTypeCombo;
//	private Text trainSizeTxt;
	private ModelChooserButton baseModelBtn;
	private Button advancedParsBtn;
	private HtrTagsToIgnoreChooserComposite tagSelectionComp;
	private ReverseTextParamsComposite reverseTextParamsComp;
	private CustomTagParamsComposite customTagParamsComp;
	private Button useExistingLinePolygonsBtn;
	private Button doBinarizeBtn;
	private Button doNotDeleteWorkDirBtn;
	
	TextFeatsCfg textFeatsCfg = new TextFeatsCfg();
	TrpPreprocPars trpPreprocPars = new TrpPreprocPars();
	PyLaiaCreateModelPars createModelPars = PyLaiaCreateModelPars.getDefault();
	PyLaiaTrainCtcPars trainCtcPars = PyLaiaTrainCtcPars.getDefault();
//	int batchSize = PyLaiaTrainCtcPars.DEFAULT_BATCH_SIZE;
	
	public PyLaiaTrainingConfComposite(Composite parent, boolean enableBaseModelSelection, int style) {
		super(parent, style);
//		setLayout(new GridLayout(2, false));
		setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		ScrolledComposite sc = new ScrolledComposite(this, SWT.V_SCROLL | SWT.H_SCROLL);
		sc.setLayoutData(new GridData(GridData.FILL_BOTH));
		sc.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		
		Composite content = new Composite(sc, SWT.NONE);
		content.setLayout(new GridLayout(2, false));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Label numEpochsLbl = new Label(content, SWT.NONE);
		numEpochsLbl.setText("Max-nr. of Epochs:");
		numEpochsTxt = new Text(content, SWT.BORDER);
		numEpochsTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		numEpochsTxt.setToolTipText("The maximum number of epochs, if early stopping does not apply");
		
		Label earlyStoppingLbl = new Label(content, SWT.NONE);
		earlyStoppingLbl.setText("Early Stopping: ");
		earlyStoppingTxt = new Text(content, SWT.BORDER);
		earlyStoppingTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		earlyStoppingTxt.setToolTipText("Stop training early, if model does not improve for this number of epochs");
		
		//Base models are not supported for PyLaia yet
		if(enableBaseModelSelection) {
			Label baseModelLbl = new Label(content, SWT.NONE);
			baseModelLbl.setText("Base Model:");		
			baseModelBtn = new ModelChooserButton(content, true, ModelUtil.TYPE_TEXT, getProvider()) {
				@Override protected void onModelSelectionChanged(TrpModelMetadata selectedModel) {
					earlyStoppingLbl.setVisible(selectedModel==null);
					earlyStoppingTxt.setVisible(selectedModel==null);
				}
			};
			baseModelBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		} else {
			baseModelBtn = null;
		}		
		
//		Group advancedParsGroup = new Group(this, 0);
//		advancedParsGroup.setText("Advanced parameters");
//		advancedParsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
//		advancedParsGroup.setLayout(new GridLayout(2, false));

		Label learningRateLbl = new Label(content, SWT.NONE);
		learningRateLbl.setText("Learning Rate:");
		learningRateTxt = new Text(content, SWT.BORDER);
		learningRateTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		Label imgTypeLbl = new Label(content, 0);
		imgTypeLbl.setText("Image type:");
		imgTypeCombo = new Combo(content, SWT.READ_ONLY | SWT.DROP_DOWN);
		imgTypeCombo.add("Originals");
		imgTypeCombo.add("Compressed");
		imgTypeCombo.select(0);
		imgTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		Composite checkboxesCont = new Composite(content, 0);
		checkboxesCont.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		checkboxesCont.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		
		doBinarizeBtn = new Button(checkboxesCont, SWT.CHECK);
		doBinarizeBtn.setText("Perform binarization");
		doBinarizeBtn.setToolTipText("Perform a binarization of the input data on line level - using no binarization can lead to better results for homogeneous training data.\nHowever, it could also lead to much worse results if training pages with varying backgorund color are used.");
		doBinarizeBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		doBinarizeBtn.setSelection(isDoBinarize());
		SWTUtil.onSelectionEvent(doBinarizeBtn, e -> setDoBinarize(doBinarizeBtn.getSelection()));
		
		useExistingLinePolygonsBtn = new Button(checkboxesCont, SWT.CHECK);
		useExistingLinePolygonsBtn.setText("Use existing line polygons for training");
		useExistingLinePolygonsBtn.setToolTipText("Line polygon will *not* be computed during training but the existing ones are used.\nDuring recognition, similar line polygons should be used for best performance of the model");
		useExistingLinePolygonsBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));		
		
		// TODO: add tags to omit and reversion combo...
		Label omitLinesByTagLabel = new Label(content, SWT.NONE);
		omitLinesByTagLabel.setText("Omit lines by tag:");
		tagSelectionComp = new HtrTagsToIgnoreChooserComposite(content, SWT.NONE);
		tagSelectionComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		reverseTextParamsComp = new ReverseTextParamsComposite(content, 0);
		reverseTextParamsComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));	
		
		customTagParamsComp = new CustomTagParamsComposite(content, 0);
		customTagParamsComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));	
		
		Composite btnCont = new Composite(content, 0);
		btnCont.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		btnCont.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		
		advancedParsBtn = new Button(btnCont, SWT.PUSH);
		advancedParsBtn.setText("Advanced parameters...");
		advancedParsBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		SWTUtil.onSelectionEvent(advancedParsBtn, e -> {
			PyLaiaAdvancedConfDialog d = new PyLaiaAdvancedConfDialog(getShell(), /*batchSize,*/ textFeatsCfg, trpPreprocPars, createModelPars, trainCtcPars);
			if (d.open() == IDialogConstants.OK_ID) {
//				batchSize = d.getBatchSize();
				textFeatsCfg = d.getTextFeatsCfg();
				trpPreprocPars = d.getTrpPreprocPars();
				createModelPars = d.getModelPars();
				trainCtcPars = d.getTrainPars();
//				logger.info("batch size = "+batchSize);
				if (textFeatsCfg != null) {
					logger.info("preprocessing config (textFeats) = "+textFeatsCfg.toSingleLineConfigString());	
				}
				if (trpPreprocPars != null) {
					logger.info("preprocessing config (trp) = "+trpPreprocPars.toJson());	
				}
				
				logger.info("modelPars = "+createModelPars.toSingleLineString());
				logger.info("trainPars = "+trainCtcPars.toSingleLineString());
				logger.info("isDoBinarize() = "+isDoBinarize());
				doBinarizeBtn.setSelection(isDoBinarize());
			}
		});
		
//		if (false) {
//		Label trainSizeLbl = new Label(this, SWT.NONE);
//		trainSizeLbl.setText("Train Size per Epoch:");
//		trainSizeTxt = new Text(this, SWT.BORDER);
//		trainSizeTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
//		}

		setDefaults();

//		new Label(this, SWT.NONE);
		Button resetBtn = new Button(btnCont, SWT.PUSH);
		resetBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		resetBtn.setText("Reset to defaults");
		resetBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				setDefaults();
			}
		});
		
		if (Storage.getInstance().isAdminLoggedIn()) {
			doNotDeleteWorkDirBtn = new Button(content, SWT.CHECK);
			doNotDeleteWorkDirBtn.setText("Do not delete workdir (for testing)");
			doNotDeleteWorkDirBtn.setToolTipText("Workdir on worker module does not get deleted - for testing purposes - use with care!");
			doNotDeleteWorkDirBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));			
		}
		
		sc.setContent(content);
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
		content.layout();
	    sc.setMinSize(0, content.computeSize(SWT.DEFAULT, SWT.DEFAULT).y-1);	    
	}
	
	public void setDefaults() {
		numEpochsTxt.setText("" + PyLaiaTrainCtcPars.DEFAULT_MAX_EPOCHS);
		earlyStoppingTxt.setText(""+ PyLaiaTrainCtcPars.DEFAULT_MAX_NONDECREASING_EPOCHS);
//		learningRateTxt.setText(""+PyLaiaTrainCtcPars.DEFAULT_LEARNING_RATE);
		learningRateTxt.setText(CoreUtils.formatDoubleNonScientific(PyLaiaTrainCtcPars.DEFAULT_LEARNING_RATE));
//		if (trainSizeTxt!=null) {
//			trainSizeTxt.setText(""+PyLaiaTrainCtcPars.DEFAULT_BATCH_SIZE);	
//		}
		if(baseModelBtn != null) {
			baseModelBtn.setModel(null);
		}
		imgTypeCombo.select(0);
		tagSelectionComp.clearSelection();
		reverseTextParamsComp.setDefaults();
		customTagParamsComp.setDefaults();
	}
	
	public List<String> validateParameters(List<String> errorList) {
		if(errorList == null) {
			errorList = new ArrayList<>();
		}
		if (!StringUtils.isNumeric(numEpochsTxt.getText())) {
			errorList.add("Number of Epochs must be a number!");
		}
		if (!StringUtils.isNumeric(earlyStoppingTxt.getText())) {
			errorList.add("Early stopping must be a number!");
		}
//		if (trainSizeTxt!=null) {
//			if (!StringUtils.isNumeric(trainSizeTxt.getText())) {
//				errorList.add("Train size must be a number!");
//			}			
//		}
		if (!CoreUtils.isDouble(learningRateTxt.getText())) {
			errorList.add("Learning rate must be a floating point number!");
		}
		
		return errorList;
	}

	public PyLaiaHtrTrainConfig addParameters(PyLaiaHtrTrainConfig conf) {
		conf.getModelMetadata().setProvider(this.getProvider());
		
		// important: set advanced preprocessing, model and train pars here, s.t. the "main" pars such as learning rate etc. can override those maybe set also in the advanced dialog... 
		if (textFeatsCfg !=null) {
			conf.setTextFeatsCfg(textFeatsCfg);
		}
		else if (trpPreprocPars != null) {
			conf.setTrpPreprocPars(trpPreprocPars);
		}
		// this should never happen:
//		else {
//			conf.setTextFeatsCfg(new TextFeatsCfg());
//		}
		conf.setCreateModelPars(createModelPars);
		conf.setTrainCtcPars(trainCtcPars);
		
		// those are the "main" parameters:
//		conf.setBatchSize(batchSize); // now set in advanced pars dialog
		conf.setNumEpochs(Integer.parseInt(numEpochsTxt.getText()));
		conf.setEarlyStopping(Integer.parseInt(earlyStoppingTxt.getText()));
//		if (trainSizeTxt!=null) {
//			conf.setBatchSize(Integer.parseInt(trainSizeTxt.getText()));	
//		}
		conf.setLearningRate(Double.parseDouble(learningRateTxt.getText()));
		conf.setTagsToOmit(tagSelectionComp.getSelectedTags());
		conf.setReverseTextParams(reverseTextParamsComp.getParams());
		conf.setCustomTagParams(customTagParamsComp.getParams());
		
		if(baseModelBtn != null) {
			conf.getModelMetadata().setBaseModelId(baseModelBtn.getModelId());
			if (baseModelBtn.getModelId() == null) {
				logger.debug("No base HTR selected.");
			}
		}

		ImgType imgType = imgTypeCombo.getSelectionIndex()==1 ? ImgType.view : ImgType.orig;
		logger.debug("imgType = "+imgType);
		conf.setImgType(imgType);
		logger.debug("useExistingLinePolygons = "+useExistingLinePolygonsBtn.getSelection());
		conf.setUseExistingLinePolygons(useExistingLinePolygonsBtn.getSelection());
		
		if (SWTUtil.getSelection(doNotDeleteWorkDirBtn)) {
			conf.setDoNotDeleteWorkDir(true);
		}
		
		return conf;
	}
	
	private boolean isDoBinarize() {
		if (textFeatsCfg!=null) {
			return textFeatsCfg.isEnh();
		}
		else if (trpPreprocPars!=null) {
			return trpPreprocPars.isDo_sauvola();
		}
		else { // should not happen...
			logger.error("Neither textFeatsCfg nor trpPreprocPars is != null -> returning true for isDoBinarize");
			return true;
		}
	}
	
	private void setDoBinarize(boolean doBinarize) {
		if (textFeatsCfg!=null) {
			textFeatsCfg.setEnh(doBinarize);
		}
		if (trpPreprocPars!=null) {
			trpPreprocPars.setDo_sauvola(doBinarize);
		}
	}

	public String getProvider() {
		return ModelUtil.PROVIDER_PYLAIA;
	}
}
