package eu.transkribus.swt_gui.htr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.chips.Chips;

import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.IsoLangUtils;
import eu.transkribus.swt.util.Colors;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.SWTUtil;

/**
 * NOTE: not finished and this not used
 * Idea: more user friendly ISO lang selection widget than IsoLanguageTable
 */
public class IsoLanguageChipsComposite extends Composite {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(IsoLanguageChipsComposite.class);
	
	Composite chipsComp;
	List<Chips> chips = new ArrayList<>();
	Text addLangText;
	SimpleContentProposalProvider scp;
	List<String> availableLangsPlusIsoCode;
	String languageString;
	
	public IsoLanguageChipsComposite(Composite parent, String languageString) {
		super(parent, 0);
		this.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		
		addLangText = new Text(this, SWT.SINGLE | SWT.BORDER);
		addLangText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		initAutoCompletion(addLangText, null);
		addLangText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setProposals(addLangText.getText());
			}
		});
		addLangText.setMessage("Type language to add...");
		addLangText.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent e) {
				e.doit = false;
				if (e.detail == SWT.TRAVERSE_RETURN) {
					addLanguage();
				}
			}
		});		
		
		chipsComp = new Composite(this, SWT.NONE);
		chipsComp.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
		chipsComp.setLayout(new GridLayout(1, false));
		chipsComp.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		setLanguageString(languageString);
		initChips();
	}
	
	private void initChips() {
		List<String> langs = getCurrentLanguages();
		logger.debug("parsed "+langs.size()+" languages");
		for (String iso : langs) {
			String lang = IsoLangUtils.DEFAULT_RESOLVER.resolveLabelFromCode(iso);
			addChip(iso, lang);
		}
	}
	
	private void initAutoCompletion(Text text, String value) {
		availableLangsPlusIsoCode = getAvailableLanguageListWithLeadingIsoCodeSorted();
		ContentProposalAdapter adapter = null;
		String[] defaultProposals = getProposals(value);
		
		scp = new SimpleContentProposalProvider(defaultProposals);
		scp.setProposals(defaultProposals);
//			KeyStroke ks = KeyStroke.getInstance("Ctrl+Space");
		adapter = new ContentProposalAdapter(text, new TextContentAdapter(), scp, null, null);
		adapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);
	}
	
	private List<String> getAvailableLanguageListWithLeadingIsoCodeSorted() {
		List<String> l = new ArrayList<>();
		Map<String, String> map = IsoLangUtils.DEFAULT_RESOLVER.getCodeToLabelMap();
		for (String iso : map.keySet()) {
			l.add(iso+" - "+map.get(iso));
		}
		Collections.sort(l);
		return l;
	}
	
	private String[] getProposals(String value) {
		List<String> props = availableLangsPlusIsoCode;
		if (!StringUtils.isEmpty(value)) {
			props = availableLangsPlusIsoCode.stream().filter(l -> l.toLowerCase().contains(value.toLowerCase())).collect(Collectors.toList());
		}
		return props.toArray(new String[0]);
	}	
	
	private void setProposals(String langText) {
//		logger.info("setProposals, langText = "+langText);
		scp.setProposals(getProposals(langText));
	}
	
	public void setLanguageString(String languageString) {
		this.languageString = languageString==null ? "" : languageString;
//		List<String> langs = getCurrentLanguages();
//		logger.debug("parsed "+langs.size()+" languages");
//		for (String iso : langs) {
//			String lang = IsoLangUtils.DEFAULT_RESOLVER.resolveLabelFromCode(iso);
//			addChip(iso, lang);
//		}
	}
	
	public String getLanguageString() {
		return this.languageString;
	}
	
	public List<String> getCurrentLanguages() {
		return CoreUtils.parseStringList(languageString, ",", true, true);
	}	
	
	private void addLanguage() {
		String langStr = addLangText.getText();
		if (StringUtils.isEmpty(langStr)) {
			return;
		}
		
		String iso = langStr.split("-")[0].trim();
		String lang = IsoLangUtils.DEFAULT_RESOLVER.resolveLabelFromCode(iso);
		if (lang == null) {
			if (iso.contains(",")) {
				DialogUtil.showErrorMessageBox(getShell(), "No commas allowed", "No commas allowed are allowed for custom languages!");
				return;
			}	
			iso = langStr.trim();
			int answer = DialogUtil.showYesNoDialog(getShell(), "ISO code not found", "The ISO-639-2 code for language '"+iso+"' was not found - do you want to add it as a custom language?");
			if (answer != DialogUtil.YES) {
				return;
			}
		}		
		
		Chips chip = new Chips(chipsComp, SWT.CLOSE);
		chip.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
		chip.setChipsBackground(Colors.getSystemColor(SWT.COLOR_BLUE));
		chip.setText(iso);
		chip.setToolTipText(lang == null ? "custom language" : lang);
		chip.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
		chips.add(chip);
		chipsComp.layout(true);
		chip.addCloseListener(e -> {
			final Chips c = (Chips) e.widget;
			chips.remove(c);
			removeLanguage(c.getText());
			c.dispose();
			chipsComp.layout(true);
		});
	}
	
	private void addChip(String iso, String lang) {
		Chips chip = new Chips(chipsComp, SWT.CLOSE);
		chip.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
		chip.setChipsBackground(Colors.getSystemColor(SWT.COLOR_BLUE));
		chip.setText(iso);
		chip.setToolTipText(lang == null ? "custom language" : lang);
		chip.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
		chips.add(chip);
		chipsComp.layout(true);
		chip.addCloseListener(e -> {
			final Chips c = (Chips) e.widget;
			chips.remove(c);
			removeLanguage(c.getText());
			c.dispose();
			chipsComp.layout(true);
		});
	}
	
	private void removeLanguage(String iso) {
		List<String> langs = getCurrentLanguages();
		langs.remove(iso);
		setLanguageString(CoreUtils.join(langs));
	}

}
